# MTTL

This application provides access to the ***Master Training Task List*** (**MTTL**)  explicating work role expectations across the USAF, and especially for the 90th Cyber Operations Squadron, as defined by US Cyber Command and other relevant stakeholders.  
The MTTL provides a listing of the *Knowledge*, *Skills*, *Abilities*, and *Tasks* (***KSATs***) that should be mastered for various workroles across the 90th and other USAF Squadrons.  

The **MTTL** provides an overview of work role requirements and mappings to training and evaluation resources related to those work role requirements.  
The **MTTL** is intended as a resource for those designing and implementing training and evaluation i.e. at the 39th and the 90th.  
The **MTTL** can also support flight commanders in understanding crew readiness,  capabilities, and training expectations given work role requirements.   
 
--- 

The legacy / production build for the Angular MTTL is located [here](https://90cos-cty-training-systems-angular-mttl.90cos.com/mttl)


The development build has more up to date and more thorough MTTL documentation [here](https://90cos-mttl.90cos.com/documentation)

--- 

## Overview of the MTTL project 
The full documentation for the Angular MTTL project is [here](https://90cos-cyt-training-systems-angular-mttl.90cos.com/documentation)

The MTTL Project:
1. provides a landing page pointing to training, evaluation, and other resources to help better understand work role requirements 
2. provides a searchable database detailing important characteristics of work role requirements 
3. organizes work role requirements and pathways into understandable components (visually and logically)


The MTTL project is essentially constituted from:
1. ExpressJS (i.e. the server.js file) for API creation and communicating with external websites and databases cf. [here](https://www.tutorialspoint.com/expressjs/expressjs_quick_guide.htm)
    * Note - Typescript is preferred over Javascript 
    * Calls to ExpressJS are defined in api.service.ts 
    * Calls to api.service.ts are made in components as needed 
2. Angular functionality 
    * The overarching app is the app.component.ts 
    * Apps are organized under the `src/app/` directory
    * Apps will typically only require editing of *.ts (not spec) and *.html files  
    * More about Angular can be found [here](https://angular.io/guide/component-overview)  


---
## To contribute to the MTTL (Master Training Task List) itself:
0. Submit your issue using the `Add KSAT` link on the top right of the page. 
1. Use spellcheck. 
2. Pay attention to required formatting. 

### If manually creating an issue in GitLab 
1. Review the schema to ensure you have the necessary fields for your contribution, please see the following link: https://gitlab.com/90cos/mttl/-/wikis/home
2.  a) Click the "Issues" button on the left side bar.

	b) Create a new issue by clicking the "New Issue" button.

3.  Select the template that best matches the changes that you are proposing from the "Description" dropdown.

    a) To propose a new work role, select the "New_Work_Role" template.
    
    b) To propose general changes, select the "General_Contribution" template.
4.  a) Provide a descriptive and unique title.

	b) Replace the text wrapped in parentheses with the required information.
	
	c) Provide a Label in the "Labels" dropdown.

--- 
## For developers 
### To build locally: 
1. Follow [these instructions](https://90cos-mttl.90cos.com/documentation/dev/set-up-env.html) to get Docker running with VSCode
1. Clone this repo 
1. Launch VSCode within the newly cloned directory and select "run in container" (this step may take a minute)
1. Press ctrl+` or select View->Terminal
1. In the terminal, run `bash build-frontend.sh`
1. If everything was done correctly, a local build of the mttl should be available at http://localhost:5000
1. To check Python linting: `flake8 --extend-ignore=E402 --exclude=./mttl/node_modules`  
1. Use `autopep8 --in-place yourfile.py` for some automated fixes
1. To check Angular linting: `cd mttl ; ng l ` or `cd mttl ; ng l --fix`  

