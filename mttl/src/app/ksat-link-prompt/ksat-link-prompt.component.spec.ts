import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KsatLinkPromptComponent } from './ksat-link-prompt.component';

describe('KsatLinkPromptComponent', () => {
  let component: KsatLinkPromptComponent;
  let fixture: ComponentFixture<KsatLinkPromptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KsatLinkPromptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KsatLinkPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
