import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '../api.service';
import { ProfileService } from '../profile.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of, from, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { faSync } from '@fortawesome/free-solid-svg-icons';

export interface DialogData {
  ksatId: string;
  type: string;
  url: string;
}

@Component({
  selector: 'app-ksat-link-prompt',
  templateUrl: './ksat-link-prompt.component.html',
  styleUrls: ['./ksat-link-prompt.component.scss']
})
export class KsatLinkPromptComponent {

  public spam: string[];
  public spamFlag: boolean;
  public url: string;
  error = false;
  thanks = false;
  safety;
  syncIcon = faSync;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<KsatLinkPromptComponent>,
    private apiService: ApiService,
    private profileService: ProfileService,
    private cookieService: CookieService,
    private http: HttpClient) { }


  requestKsatLink(url, type, ksatID, safety): string {
    const token = this.cookieService.get('ProfileforMTTL');
    console.log('newLink/' + ksatID + '/' + type + '/' + token +
    '/' +  encodeURIComponent(safety) + '/' +  encodeURIComponent(url));
    this.http.get('newLink/' + ksatID + '/' + type + '/' + token +
                           '/' +  encodeURIComponent(safety) + '/' +  encodeURIComponent(url), { responseType: 'text' })
                           .subscribe( data=> {
                             console.log('url after data pull: ', data);
                             this.url = data;
                             this.spamCheck(this.url);
                           });
    // res is a string that has been sent from server
    return this.url;
  }


  getSafetyStatus(url): Observable<any> {
    console.log('ksat link getting safety for ', url);
    return this.http.get('checkUrlSafety/' + url );
  }

  async parseSafetyStatus(url) {
    const response = await this.getSafetyStatus(url).toPromise();
    return response;
  }

  spamCheck(val: string): boolean{
    console.log('in spamCheck with: ', val);
    if(val){
      this.spam = val.match(/\*\*flag\*\*/gi);
      if(this.spam && this.spam.length > 0){
        this.spamFlag=true;
        console.log('Spam response');
      }
    }else{
      this.spamFlag=false;
    }
   return this.spamFlag;
  }

  click(url): void{
    if(!this.spamFlag){
      window.open(url, '_blank');
    }
  }

  reset(): void{
    this.error = false;
    this.thanks = false;
    this.spam = [''];
    this.spamFlag = false;
    this.safety = '';
    this.url = '';
    return;
  }

  sendData(): any {
    console.log('inside sendData');
    if (this.thanks === true){
      this.reset();
    }
    this.error = false;
    this.thanks = true;
    const url = this.data.url;
    if (url.includes('confluence.90cos.cdl.af.mil')) {
      Promise.all([
        this.safety = 'confluence'
      ]).then(data => {
        const tmp = this.requestKsatLink(url, this.data.type, this.data.ksatId, this.safety);
        this.thanks = true;
      });
    } else if (url) {
      Promise.all([
        this.parseSafetyStatus(url),
        this.apiService.getUrlStatus(url)
      ]).then(data => {
        console.log('Component - data received - safety', data[0]);
        console.log('Component - data received - url status', data[1]);
        this.safety = JSON.stringify(data[0]);
        if (data[1]) {
          this.thanks = true;
          console.log('Going to api service...');
          this.requestKsatLink(url, this.data.type, this.data.ksatId, this.safety);
        } else {
          this.error = true;
        }
      });
    }
  }
}

