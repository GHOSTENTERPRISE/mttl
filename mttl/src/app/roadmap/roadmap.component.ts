import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import * as halfmoon from 'halfmoon';

import { faReply, faMapSigns, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-roadmap',
  templateUrl: './roadmap.component.html',
  styleUrls: ['./roadmap.component.scss']
})
export class RoadmapComponent implements OnInit {

  // Dict of roadmaps will be stored here.
  workroleRoadmap;

  // Current hovered step on wr roadmap
  currentStep;
  // Fa Icons
  backArrow = faReply;
  sign = faMapSigns;
  linkIcon = faExternalLinkAlt;

  // Controller for indv. work-role roadmap entry
  enteredWRRoadmap = false;

  workroleInstructionsShown = false;

  currentEntry: Record<string, any>;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.workroleRoadmap = this.apiService.getRoadmap();
    this.currentEntry = this.workroleRoadmap.find( wr => wr.id === '90COS');
    // Default on ready function for halfmoon UI
    halfmoon.onDOMContentLoaded();
    halfmoon.initStickyAlert({
      content: '<ul><li>Hover over work roles for more information.</li><li>Click to view training paths for that work role</li></ul>',
      title: 'I\'m Interactive!',
      fillType: 'filled',
      alertType: 'alert-primary',
      timeShown: 3000
    });
  }

  setRoadmap(value): void {
    this.currentEntry = value;
  }

  setStep(value): void {
    this.currentStep = value;
  }

  loadRoadmap(e){
    if (this.currentEntry.paths.length > 0) {
      if (!this.workroleInstructionsShown){
        this.workroleInstructionsShown = ! this.workroleInstructionsShown;
        halfmoon.initStickyAlert({
          content: `<ul><li>Hover over road map steps for more information.</li>
                    <li>Click to view relevant links for that work role step</li></ul>`,
          title: 'Work Role Roadmaps',
          fillType: 'filled',
          alertType: 'alert-primary',
          timeShown: 3000
        });
      }
      this.enteredWRRoadmap = e;
    } else {
      halfmoon.initStickyAlert({
        content: 'Training paths for ' + this.currentEntry.fullName + ' are not yet available.',
        title: 'Currently Unavailable',
        alertType: 'alert-danger',
        fillType: 'filled'
      });
    }
  }

}
