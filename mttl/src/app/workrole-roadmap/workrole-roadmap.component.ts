import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-workrole-roadmap',
  templateUrl: './workrole-roadmap.component.html',
  styleUrls: ['./workrole-roadmap.component.scss']
})
export class WorkroleRoadmapComponent implements OnInit, OnChanges {
  @Input () inputData: Record<string, any>;
  @Output() private hoveredStep = new EventEmitter<Record<string, unknown>>();
  private canvas: any;
  private data: Record<string, any>;
  private arrowSize = 500;
  private yTransform = 50;
  private xTransform = 50;
  private triangle = d3.symbol()
            .type(d3.symbolTriangle)
            .size(this.arrowSize);
  private innerRadius = 45;
  private outerRadius = 55;
  private roadmapArc = d3.arc()
            .innerRadius(this.innerRadius)
            .outerRadius(this.outerRadius)
            .startAngle(0)
            .endAngle(180 * (Math.PI/180));
  private diamOffset = 2 * ((this.innerRadius + this.outerRadius) / 2);
  private roadOffset = this.yTransform - 19;
  private viewWidth = 750;
  private viewHeight: number;
  private points = '50,5   100,5  125,30  125,80 100,105 50,105  25,80  25, 30';
  constructor() { }

  ngOnInit(): void {
    console.log (this.inputData);
    this.data = this.inputData.paths[0].path;

    this.viewHeight = this.diamOffset * (this.data.length / 2) + 50;
    const instructions: any = {};
    instructions.link='';
    instructions.name=this.inputData.name + ' Roadmap';
    instructions.description = 'Hover over any step to see more information. Most steps are clickable and will guide you to relevant links';
    this.hoveredStep.emit(instructions);
    this.canvas = this.makeSVG();
    this.drawRoadmap(this.canvas);
  }

  ngOnChanges(): void {

  }

  private makeSVG() {
    const svg = d3.select('.roadmap').append('svg')
        .attr('width', '100%')
        .attr('height', '100%')
        .attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('viewBox', '0 0 ' + this.viewWidth + ' ' + this.viewHeight)
        .style('background-color', '#eee');
    const g = svg.append('g')
        .attr('class', 'g');
    //.attr("transform", "translate(" + this.width/2 + "," + this.margin.top + ")");

    const zoom = d3.zoom().on('zoom', e => {
        g.attr('transform', e.transform);
    });

    // svg.call(zoom);
    // Disabling zooming and panning on this page
    return g;
  }

  private drawRoadmap(g){

    const startArrow = g.append('g')
        .attr('class', 'startArrow');

    startArrow.append('rect')
              .attr('height', 10)
              .attr('width', 100)
              .attr('fill', '#707070')
              .attr('y', this.roadOffset)
              .attr('x', this.xTransform);
    startArrow.append('path')
              .attr('d', this.triangle)
              .attr('stroke', '#2D5C79')
              .attr('stroke-width', '2')
              .attr('fill', '#F0F7FD')
              .attr('transform', 'rotate(-30) translate(15 ' + this.yTransform +')');

    let xPos = 100;
    let yPos = 0;
    let direction = 1;
    this.data.forEach( (element, index) => {
      this.createBox(xPos, yPos, element, g);
      xPos += 250 * direction;
      if (index % 2 === 1 && (index !== this.data.length - 1)){
        if (direction > 0){
          this.fillerRect(xPos + this.xTransform - 5, yPos + this.roadOffset, g);
          g.append('path')
           .attr('d', this.roadmapArc)
           .attr('transform', 'translate(' + (xPos + this.xTransform + 20) + ',' + (yPos + + this.roadOffset + this.outerRadius) +')')
           .attr('fill', '#707070');
          this.fillerRect(xPos + this.xTransform - 5, yPos + this.roadOffset + this.diamOffset, g);
          xPos -= 250; // shift to accommodate change in direction
        } else if (  (index !== this.data.length - 1) ){
          xPos += 250;  // shift to accommodate change in direction
          this.fillerRect(xPos + this.xTransform - 21, yPos + this.roadOffset, g);
          g.append('path')
           .attr('d', this.roadmapArc)
           .attr(
             'transform', 'translate(' + (xPos + this.xTransform - 20) + ',' +
             (yPos + this.roadOffset + this.outerRadius) +') rotate(180)'
          )
           .attr('fill', '#707070');
          this.fillerRect(xPos + this.xTransform - 21, yPos + this.roadOffset + this.diamOffset, g);
        }
        direction = direction * -1;
        yPos += this.diamOffset;
      }
    });

    if (direction > 0){
      // Line connecting Roadmap to stopsign
      g.append('rect')
       .attr('height', 10)
       .attr('width', 75)
       .attr('fill', '#707070')
       .attr('transform', 'translate(' + (xPos + this.xTransform - 1) + ',' + (yPos + this.roadOffset) + ')');
      // Stopsign
      g.append('polygon')
      .attr('points', this.points)
      .attr('stroke', '#fff')
      .attr('stroke-width', 3)
      .attr('fill', '#cf142b')
      .attr('transform', 'translate(' + (xPos + this.xTransform) + ',' + (yPos + this.yTransform - 70) + ')');
      // STOP text
      g.append('text')
       .text('STOP')
       .attr('fill', '#FFF')
       .style('font-size', '20pt')
       .style('font-weight', 'bolder')
       .attr('pointer-events', 'none')
       .style('text-anchor', 'middle')
       .attr('x', (xPos + this.xTransform) + 75)
       .attr('y', yPos + 43);
    } else if (direction < 0) {
      this.fillerRect(xPos + this.xTransform + 225, yPos + this.roadOffset, g);
      g.append('polygon')
      .attr('points', this.points)
      .attr('stroke', '#fff')
      .attr('stroke-width', 3)
      .attr('fill', '#cf142b')
      .attr('transform', 'translate(' + (xPos + this.xTransform + 100) + ',' + (yPos + this.yTransform - 70) + ')');
      // STOP text
      g.append('text')
       .text('STOP')
       .attr('fill', '#FFF')
       .style('font-size', '20pt')
       .style('font-weight', 'bolder')
       .attr('pointer-events', 'none')
       .style('text-anchor', 'middle')
       .attr('x', (xPos + this.xTransform) + 175)
       .attr('y', yPos + 43);
    }

  }

  private createBox(x, y, element, canvas){
    const hoverEvent = this.hoveredStep;
    const xOffset = 25;
    canvas.append('rect')
          .attr('height', 10)
          .attr('width', 255)
          .attr('fill', '#707070')
          .attr('x', x + this.xTransform - 5)
          .attr('y', y + this.roadOffset);
    const stepGroup = canvas.append('g');
    stepGroup.append('rect')
             .attr('id', element.name.replace(/\s/g, ''))
             .attr('height', 30)
             .attr('width', 200)
             .attr('x', x + this.xTransform + xOffset)
             .attr('y', y + this.yTransform - 30)
             .attr('stroke', '#2D5C79')
             .attr('stroke-width', '2')
             .attr('fill', '#F0F7FD')
             .on('mouseover', function(d, i) {
               d3.select(this).transition()
                    .duration(500)
                    .style('stroke', '#000')
                    .attr('height', 90);
               return hoverEvent.emit(element);
             })
             .on('mouseout', function(d, i) {
                d3.select(this).transition()
                    .duration(500)
                    .style('stroke', '#2D5C79')
                    .attr('height', 30);
             })
            .on('click', d => {
              if (element.link.length > 0 && element.link !== '#'){
                window.open(
                  element.link,
                  '_blank'
                );
              }
            });

    // Clip path
    stepGroup.append('clipPath')
             .attr('id', 'text-clip-' + element.name.replace(/\s/g, ''))
             .append('use')
             .attr('xlink:href', '#' + element.name.replace(/\s/g, ''));
    // Title
    stepGroup.append('text')
          .text(element.name)
          .attr('x', x + this.xTransform + 125)
          .attr('y', y + this.yTransform - 10)
          .attr('pointer-events', 'none')
          .style('font-family', 'sans-serif')
          .style('text-anchor', 'middle')
          .style('fill', '#2D5C79')
          .style('font-size', 'small')
          .style('font-weight', 'bold');

    // Description text is hidden until rect expands
    stepGroup.append('text')
             .attr('pointer-events', 'none')
             .style('font-size', 'x-small')
             .style('font-family', 'sans-serif')
             .attr('x', x + this.xTransform + xOffset + 10)
             .attr('y', y + this.yTransform + 16)
             .attr('clip-path', 'url(#text-clip-' + element.name.replace(/\s/g, '') + ')')
             .text(element.description)
             .call(this.wordWrap, 180, element.name.replace(/\s/g, ''));
  }

  private fillerRect(x, y, g){
    g.append('rect')
            .attr('height', 10)
            .attr('width', 26)
            .attr('x', x)
            .attr('y', y)
            .attr('fill', '#707070');
  }

  private wordWrap(text, width, clip) {
    text.each(function() {
        const currentText = d3.select(this);
            const words = currentText.text().split(/\s+/).reverse();
            let word;
            let line = [];
            let lineNumber = 0;
            const lineHeight = 1; // ems
            const x = currentText.attr('x');
            const y = currentText.attr('y');
            const dy = 0;
            let tspan = currentText.text(null)
                        .append('tspan')
                        .attr('x', x)
                        .attr('y', y)
                        .attr('dy', dy + 'em');
        word = words.pop();
        while (word) {
            line.push(word);
            tspan.text(line.join(' '));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(' '));
                line = [word];
                tspan = currentText.append('tspan')
                            .attr('x', x)
                            .attr('y', y)
                            .attr('dy', ++lineNumber * lineHeight + dy + 'em')
                            .attr('clip-path', 'url(#text-clip-' + clip + ')')
                            .text(word);
            }
            word = words.pop();
        }
    });
}

}
