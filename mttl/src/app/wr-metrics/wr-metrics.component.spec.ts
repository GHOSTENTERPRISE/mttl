import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WrMetricsComponent } from './wr-metrics.component';

describe('WrMetricsComponent', () => {
  let component: WrMetricsComponent;
  let fixture: ComponentFixture<WrMetricsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WrMetricsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WrMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
