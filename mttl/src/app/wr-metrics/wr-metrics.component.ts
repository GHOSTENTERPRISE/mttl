import { Component, OnInit, ViewChild, ChangeDetectionStrategy, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ApiService } from '../api.service';
import { MatDialog } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { KsatLinkPromptComponent } from '../ksat-link-prompt/ksat-link-prompt.component';
import { NewKsatPopupComponent } from '../new-ksat-popup/new-ksat-popup.component';
import { CookieService } from 'ngx-cookie-service';
import * as introJs from 'intro.js/intro.js';

import { ExportDataService } from '../export-data.service';

import { faBook,
         faStopwatch,
         faUsers,
         faPlus,
         faSearch,
         faChild,
         faSortUp,
         faSortDown,
         faSort,
         faHammer,
         faToolbox,
         faHome,
         faCheckDouble,
         faChalkboardTeacher,
         faMapMarker
} from '@fortawesome/free-solid-svg-icons';

import { CoverageCalculatorPipe } from '../pipes/coverage-calculator.pipe';
import { stringify } from '@angular/compiler/src/util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wr-metrics',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './wr-metrics.component.html',
  styleUrls: ['./wr-metrics.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers:    [ CoverageCalculatorPipe ]
})
export class WrMetricsComponent implements OnInit {
  @ViewChild('filterCol') filterCol;
  @ViewChild('filterInput') filterInput;
  @ViewChild('searchInput') searchInput;

  cookieValue: string;
  introJS = introJs();

  tmp_array: string[];
  tmp_string: string;
  ksats;
  autocomplete;
  extradata;
  specs;
  currentReport = 'MTTL';
  sortDirection = '';
  sortColumn = 'wr';
  count = 0;
  page = 1;
  pageSize = 1;
  tableSize = 7;
  filterObj = [];
  allWR = ['All'];
  tableSizes = [3, 6, 9, 12];
  sortCombo = [this.sortDirection + this.sortColumn];
  wrObj = this.allWR;

  // FA Icons
  faBook = faBook;
  faStopwatch = faStopwatch;
  faUsers = faUsers;
  faPlus = faPlus;
  faSearch = faSearch;
  faChild = faChild;
  faSortUp = faSortUp;
  faSortDown = faSortDown;
  faSort = faSort;
  bothCovered = faCheckDouble;
  faTrn = faChalkboardTeacher;
  unmapped = faMapMarker;

  // Searchbar settings
  visible = true;
  selectable = false;
  removable = true;
  addOnBlur = false;
  readonly separatorKeysCodes: number[] = [COMMA, ENTER];

  // Expanded Pane Carousel Settings
  itemsPerSlide = 5;
  linksPerSlide = 7;
  singleSlideOffset = false;
  noWrap = true;

  searchFC = new FormControl ();

  //list of options that populates the 'Filter By' field in search
  filterOptions = ['Work Role', 'Tasks', 'Abilities', 'Skills', 'Knowledge'];

  /* eslint-disable */
  // ESLint doesn't like how these properties are named.
  //key value pairs to pull data into valid options of Filter By options
  //these now match the pairing that getWrMetrics pulls in. 
  filterMappings = {
      'Work Role': 'wr', //pulls proper wr options from extra_dataset json file
      Tasks: 'tasks',
      Abilities: 'abs',
      Skills: 'skill',
      Knowledge: 'know'
  };
  constructor(
    private apiService: ApiService,
    private dialog: MatDialog,
    private exportdata:   ExportDataService,
    private router: Router,
    private cookieService: CookieService
  ) { }

  getAllKSATs(): void {
    // Main function to retrieve all ksats from API service
    this.ksats = this.apiService.getAllKSATs();
  }

  getWrMetrics(): void{
    this.extradata = this.apiService.getWrMetrics();
    this.specs = Object.keys(this.extradata.wrspec).map((key) => {
      return {
        wr: this.extradata.wrspec[key].wr_spec,
        tasks: this.extradata.wrspec[key].Tasks,
        abs: this.extradata.wrspec[key].Abilities,
        skill: this.extradata.wrspec[key].Skills,
        know: this.extradata.wrspec[key].Knowledge
      }
    });

    //testing
    //this.test("Specs from getWrMetrics(): ", this.specs);
    //this.test("Specs for specs[1]: ", this.specs[1].wr);
  }


  clearChecks(radioName): void {
    // Function to clear radio buttons based on the name
    // This closes an expanded detail pane
    const ele = document.querySelector('input[name="' + radioName + '"]:checked') as HTMLInputElement;
    ele.checked = false;
  }


  addFilter(filter): void {
    const output = this.filterObj;
    let toAdd = true;
    this.uniqueValue(this.filterObj);
    //filterObj is recieved as a blank array
    //if any input is in filterObj then it will have to run through the check to verify. 
    if(this.filterObj.length > 0){
      this.filterObj.forEach(element => {
        //this.test("Filter Column in addFilter: ", filter.column);
        //this.test("Element column in addFilter: ", element.column);
        //this.test("Filter filterText in addFilter: ", filter.filterText);
        //this.test("Element column in addFilter: ", element.filterText);
        if (element.column === filter.column && element.filterText === filter.filterText){
          toAdd = false;
        }
      });
    }

    if (toAdd){
      //this.test("Adding Filter: ", filter);
      this.filterObj.push(filter);
    }
  }


  //gets called when the search button is input.
  //going to be the 'search' column and text in the form of searchInput.value for text var
  filterKSATList(column, text): void {
    let modifiedCol = '';
    if (column !== 'search'){
      modifiedCol = this.filterMappings[column];
    } else {
      modifiedCol = column;
    }
    // Adds a key value pair to the filter_obj array to be filtered on
    this.addFilter({ column: modifiedCol, filterText: text });
    this.filterInput.nativeElement.value = '';
    this.searchInput.nativeElement.value = '';
  }

  clear(){
    this.searchFC.reset();
    this.filterObj = [];
    this.searchInput.nativeElement.value = '';
    this.filterInput.nativeElement.value = '';
    this.page = 1;
  }


  searchKSATList(value): void {
    this.addFilter({ column: 'search', filterText: value });
  }


  removeFilter(column, text): void {
    // Removes specified key value pair from filter_obj
    this.filterObj.forEach( (element, index) => {
      if (element.column === column && element.filterText === text) {
        this.filterObj.splice(index, 1);
      }
    });
    this.page = 1;
  }


  ksatLinkDialog(ksat, ksatType): void {
    const dialogRef = this.dialog.open(KsatLinkPromptComponent, {
      data: {
        ksatId: ksat.ksat_id,
        type: ksatType
      }
    });

  }


  changeSort(sort): void {
    // Changes sort column and sets variable used by frontend
    if (this.sortColumn !== sort) {
      this.sortColumn = sort;
      this.sortDirection = '';
      this.sortCombo = [this.sortDirection + this.sortColumn];
    } else {
      this.changeSortDirection();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];
        const cur: any = chng.currentValue;
        const prev: any = chng.previousValue;
      }
    }
  }

  goToPage(pageName: string): void {
    const navDetails: string[] = ['../'];
    if (pageName.length) {
      navDetails.push(pageName);
    }
    this.router.navigate(navDetails);
  }


  changeSortDirection(): void {
    // Changes sort direction and handles sorting on same column (inverts results)
    if (this.sortDirection === '') { this.sortDirection = '-'; }
    else { this.sortDirection = ''; }
    this.sortCombo = [this.sortDirection + this.sortColumn];
  }

  changeWR(workRole): void {
    // Setter function for work roles
    // if All is selected, set it to predefined list
    if (workRole !== 'All') { this.wrObj = [workRole]; }
    else { this.wrObj = this.allWR; }
  }

  test (text: string, item: any): void{
    console.log(text, item);
  }


  ngOnInit(): void {
    this.getWrMetrics();
    this.autocomplete = this.apiService.getAutocomplete();
    //console.log("ngOnInit has completed")
  }


  checkboxSelect(i, name): void {
    const tabs = document.getElementsByName('tabs-' + i.toString());
    const valueKey = 'value';
    const checkedKey = 'checked';
    tabs.forEach(tab => {
      if (tab[valueKey] !== name){
        tab[checkedKey] = false;
      }
    });
  }


  descFromKsatId(ksatId): string {
    let output = '';
    this.ksats.forEach(ksat => {
      if (ksat.ksat_id === ksatId){
        output = ksat.description;
      }
    });
    return output;
  }


  getColor(ksatId: string): string {
    let color = '#000000';
    switch (ksatId[0]){
      case ('K'): {
        color = '#00abd4';
        break;
      }
      case ('S'): {
        color = '#b43000';
        break;
      }
      case ('A'): {
        color = '#b48a00';
        break;
      }
      case ('T'): {
        color = '#5d00b4';
        break;
      }
      default: {
        break;
      }
    }
    return color;
  }


  determineIcon(ksatId: string): any {
    let icon = faBook;
    switch (ksatId[0]){
      case ('K'): {
        icon = faBook;
        break;
      }
      case ('S'): {
        icon = faHammer;
        break;
      }
      case ('A'): {
        icon = faToolbox;
        break;
      }
      case ('T'): {
        icon = faHome;
        break;
      }
      default: {
        break;
      }
    }
    return icon;
  }


  getHref(html: string): string {
    try{
      const re = /href\s*=\s*"([^"]*)"/;
      return re.exec(html)[1];
    } catch {
      return '#';
    }
  }


  getAnchorText(html: string): string {
    try{
      const re = />([^<]*)</;
      const text = (re.exec(html)[1]);
      return text.replace(/[_-]/g, ' ');
    } catch {
      return 'No String';
    }
  }


  add(event: MatChipInputEvent): void {
    const value = event.value;
    const column = this.filterCol.value;
    console.log("filterCol.value : ", this.filterCol.value);
    console.log("Attempting to add search to filter");
    if (column === 'search'){
      this.searchKSATList(value);
    }
    else
    {
      try{
        this.filterKSATList(column.trim(), value.trim());
      } catch {
        console.log('Error: could not add filter');
      }
    }
    // Reset the input value
    this.filterInput.nativeElement.value = '';
  }

  addBySelection(event: MatAutocompleteSelectedEvent): void{
    this.page = 1;
    const value = event.option.viewValue;
    const column = this.filterCol.value;
    this.filterKSATList(column.trim(), value.trim());
    this.filterInput.nativeElement.value = '';
  }

  searched(col: any []): any []{
    const output = [];
    col.forEach(element => {
      if (element.column === 'search'){
        this.test("Searched col value:", element);
        output.push(element);
      }
    });
    this.test("Output of searched: ", output);
    return output;
  }

  sortSpec(filter: any): any []{
    const output = [];
    const hold = [];
    var str: string;
    this.specs.forEach(element => {
      if(element[filter]!=''){
        hold.push(element[filter]);
      }
    });
    //this.test("hold array: ",hold);
    for (var _i = 0; _i < hold.length; _i++){
      str = hold[_i];
      if(str != undefined){
        this.tmp_array = str.split(',');
        this.tmp_array.forEach(obj=>{
          output.push(obj);
        });
      }
    }
    return output;
  }

  specFilter(list: string[], filt: string): any[]{
    const output = [];
    if (list[filt].length > 5){
      output.push(list[filt].split(','));
    }else{
      output.push(list[filt]);
    }
    return output;
  }

  filtered(col: any []): any []{
    const output = [];
    col.forEach(element => {
      if (element.column !== 'search'){
        output.push(element);
      }
    });
    return output;
  }

  //col can be any object array
  //will sort and return the array after it validates uniqueness
  uniqueValue(col: any[]): any[]{
    const output = [];
    for(const val of col){
      if(!output.includes(val)){
        output.push(val);
      }
    }
    return output;
  }
}
