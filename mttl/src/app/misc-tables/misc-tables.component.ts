import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import * as halfmoon from 'halfmoon';

@Component({
  selector: 'app-misc-tables',
  templateUrl: './misc-tables.component.html',
  styleUrls: ['./misc-tables.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MiscTablesComponent implements OnInit {

  constructor() { }

  toggleDarkMode(): void {
    halfmoon.toggleDarkMode();
  }

  ngOnInit(): void {
    halfmoon.onDOMContentLoaded();
  }

}
