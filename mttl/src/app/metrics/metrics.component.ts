import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { ApiService } from '../api.service';

import filterData from '../all-metrics/filters.json';

// Pipes
import { FilterMTTLPipe } from '../pipes/filter-mttl.pipe';
import { CoverageCalculatorPipe } from '../pipes/coverage-calculator.pipe';
import { FilterByArrayPipe } from '../pipes/filter-by-array.pipe';

import * as halfmoon from 'halfmoon';
import Chart from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

import { faChartPie, faRulerHorizontal } from '@fortawesome/free-solid-svg-icons';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';


@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.scss'],
  providers: [FilterMTTLPipe, CoverageCalculatorPipe, FilterByArrayPipe],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetricsComponent implements OnInit {
  @ViewChild('filterCol') filterCol;
  @ViewChild('filterInput') filterInput;
  @ViewChild('searchInput') searchInput;
  // FA Icons
  faChartPie = faChartPie;
  faRulerHorizontal = faRulerHorizontal;

  // Main ksat list populated from API call
  ksats;
  autocomplete;
  allWr = ['All'];
  wrObj = this.allWr;

  currentReport = 'MTTL';

  filterObj = []; // Format expected: {"column": "filter_text"}

  // Chart Variables
  chartType = 'bar';
  chart: any;
  ctx: any;
  searchFC = new FormControl(null, {
    updateOn: 'blur'
  });

  filterOptions = ['Work Role', 'Topic', 'Owner', 'Source', 'Parent', 'KSAT ID'];
  autoCompleteVals = [];

  /* eslint-disable */
  // ESLint doesn't like how these properties are named.
  // gives json template filter options for key:value pairs
  filterMappings = {
    Owner: 'requirement_owner',
    Source: 'requirement_src',
    Parent: 'parent',
    'KSAT ID': 'ksat_id',
    Topic: 'topic',
    'Work Role': 'wr_spec'
  };
  // pull in the mttl component file. 

  filters = filterData;
  constructor(
    private apiService: ApiService,
    private coverageCalculator: CoverageCalculatorPipe,
    private dialog: MatDialog,
    private mttlFilter: FilterMTTLPipe,
    private arrayFilter: FilterByArrayPipe) { }

  total: number;
  covered: number;
  evalCovered: number;
  trnCovered: number;
  noCoverage: number;

  ngOnInit(): void {
    // When DOM is ready, do this:
    this.ksats = this.apiService.getAllKSATs();
    this.autocomplete = this.apiService.getAutocomplete();
    // Default on ready function for halfmoon UI
    halfmoon.onDOMContentLoaded();
    this.calculateChart();
  }

  addFilter(filter): void {
    let toAdd = true;
    this.filterObj.forEach(element => {
      if (element.column === filter.column && element.filterText === filter.filterText) {
        toAdd = false;
      }
    });

    if (toAdd) {
      this.filterObj.push(filter);
    }
  }

  tempTestFunction(thing) {
    console.log(thing);
  }
  toggleChartType(): void {
    this.chartType === 'bar' ? this.chartType = 'pie' : this.chartType = 'bar';
    this.chart.destroy();
    this.calculateChart();
  }

  toggleDarkMode(): void {
    halfmoon.toggleDarkMode();
  }

  calculateChart(): void {
    if (Object.keys(this.filterObj).length > 0) {
      if (this.filterObj[0]['column'] === 'wr_spec') {
        if (this.filterObj[0]['filterText'] === undefined || this.filterObj[0]['filterText'] === '') {
          this.filterObj[0]['filterText'] = "90COS"
        }
      }
    }

    let filteredKsats = this.mttlFilter.transform(this.ksats, this.filterObj);
    filteredKsats = this.arrayFilter.transform(filteredKsats, 'wr_spec', this.wrObj);
    this.total = filteredKsats.length;
    this.covered = this.coverageCalculator.transform(filteredKsats, 't_and_e').length;
    this.evalCovered = this.coverageCalculator.transform(filteredKsats, 'e_o').length;
    this.trnCovered = this.coverageCalculator.transform(filteredKsats, 't_o').length;
    this.noCoverage = this.coverageCalculator.transform(filteredKsats, 'none').length;
    this.ctx = document.getElementById('chart');
    if (this.chartType === 'bar') {
      this.ctx.height = 45;
      this.calculateBarChart(this.covered, this.evalCovered, this.trnCovered, this.noCoverage);
    } else {
      this.ctx.height = 65;
      this.calculatePieChart(this.covered, this.evalCovered, this.trnCovered, this.noCoverage);
    }
  }

  calculateBarChart(covered: number, evalCovered: number, trnCovered: number, noCoverage: number): void {
    const total = covered + noCoverage + trnCovered + evalCovered;
    this.chart = new Chart(this.ctx, {
      type: 'horizontalBar',
      plugins: [ChartDataLabels],
      data: {
        labels: ['KSATs Covered'],
        datasets: [
          {
            label: 'Training and Eval (' + covered + '/' + total + ')',
            data: [covered],
            backgroundColor: '#6EC664',
            borderWidth: 3,
            borderSkipped: null
          },
          {
            label: 'Eval (' + (evalCovered + covered) + '/' + total + ')',
            data: [evalCovered],
            backgroundColor: '#F0AB00',
            borderWidth: 3,
            borderSkipped: null
          },
          {
            label: 'Training (' + (trnCovered + covered) + '/' + total + ')',
            data: [trnCovered],
            backgroundColor: '#0066CC',
            borderWidth: 3,
            borderSkipped: null
          },
          {
            label: 'No Coverage (' + noCoverage + '/' + total + ')',
            data: [noCoverage],
            backgroundColor: '#707070',
            borderWidth: 3,
            borderSkipped: null
          }
        ]
      },
      options: {
        responsive: true,
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            stacked: true,
            ticks: { max: total },
            display: false,
            gridLines: {
              display: false,
            }
          }],
          yAxes: [{ display: false, stacked: true }]
        },
        legend: {
          display: false,
          labels: {
            fontSize: 20,
          }
        },
        plugins: {
          // Change options for ALL labels of THIS CHART
          datalabels: {
            color: '#ffffff',
            formatter: (value, contxt) => {
              const percentage = Math.round(( value/total ) * 100);
              if (percentage){
                return percentage + '%';
              }
              return '';
            },
            font: {
              size: 20
            }
          }
        }
      }
    });
  }

  calculatePieChart(covered: number, evalCovered: number, trnCovered: number, noCoverage: number): void {
    const total = covered + noCoverage + trnCovered + evalCovered;
    this.chart = new Chart(this.ctx, {
      type: 'doughnut',
      plugins: [ChartDataLabels],
      data: {
        labels: [
          'Training and Eval (' + Math.trunc((covered / total) * 100) + '%)',
          'Eval (' + Math.trunc((evalCovered / total) * 100) + '%)',
          'Training(' + Math.trunc((trnCovered / total) * 100) + '%)',
          'No Coverage (' + Math.trunc((noCoverage / total) * 100) + '%)'
        ],
        datasets: [

          {
            data: [covered, evalCovered, trnCovered, noCoverage],
            backgroundColor: ['#6EC664', '#F0AB00', '#0066CC', '#707070'],
            borderWidth: 1
          }
        ],
      },
      options: {
        responsive: true,
        scales: {
          xAxes: [{
            ticks: { max: total },
            display: false,
            gridLines: {
              display: false,
            }
          }],
          yAxes: [{ display: false }]
        },
        legend: {
          display: false,
          labels: {
            fontSize: 20,
          }
        },
        plugins: {
          // Change options for ALL labels of THIS CHART
          datalabels: {
            formatter: (value, contxt) => {
              const percentage = Math.round(( value/total ) * 100);
              if (percentage){
                return percentage + '%';
              }
              return '';
            },
            color: '#ffffff',
            font: {
              size: 20
            }
          }
        }
      }
    });
  }

  getReport(filterType: string, filterVal: string, wrVal): string {
    this.chart.destroy();
    this.filterObj = [];
    this.wrObj = this.allWr;

    console.log("filterType:", filterType)
    console.log(this.wrObj)
    if (filterType === 'mttl') {
      this.calculateChart();
      return 'MTTL';
    } else {
      this.filterObj.push({ column: filterType, filterText: filterVal });
      this.calculateChart();
    }

    if (filterType === 'wr_spec') {
      if (filterVal !== undefined) { return 'Work Role/Specialization: ' + filterVal; }
      else {
        filterVal = '90COS';
        return 'Work Role/Specialization: ' + filterVal;
      }
    }
    else if (filterType === 'requirement_src') { return 'Source: ' + filterVal; }
    else if (filterType === 'topic') { return 'Topic: ' + filterVal; }
    else { return 'Owner: ' + filterVal; }
  }
  searched(col: any[]): any[] {
    const output = [];
    col.forEach(element => {
      if (element.column === 'search') {
        output.push(element);
      }
    });
    return output;
  }

  filtered(col: any[]): any[] {
    const output = [];
    col.forEach(element => {
      if (element.column !== 'search') {
        output.push(element);
      }
    });
    return output;
  }
  descFromKsatId(ksatId): string {
    let output = '';
    this.ksats.forEach(ksat => {
      if (ksat.ksat_id === ksatId) {
        output = ksat.description;
      }
    });
    return output;
  }
  searchKSATList(value): void {
    // Adds a special key value pair with the "search" column prepopulated
    // This notifies the filter pipe to do an expensive full text search on every result
    // this.filter_obj.push({ 'column': 'search', 'filter_text': this.search_input.nativeElement.value })
    this.addFilter({ column: 'search', filterText: value });
    // console.log('search value:', this.search_input.nativeElement.value);
  }
  filterKSATList(column, text): void {
    let modifiedCol = '';
    if (column !== 'search') {
      modifiedCol = this.filterMappings[column];
    } else {
      modifiedCol = column;
    }

    // Adds a key value pair to the filter_obj array to be filtered on
    this.addFilter({ column: modifiedCol, filterText: text });
    this.filterInput.nativeElement.value = '';
    this.searchInput.nativeElement.value = '';
  }

  removeFilter(column, text): void {
    // Removes specified key value pair from filter_obj
    this.filterObj.forEach((element, index) => {
      if (element.column === column && element.filterText === text) {
        this.filterObj.splice(index, 1);
      }
    });
  }

  clickableFilter(filterColumn, text): void {
    this.addFilter({ column: filterColumn, filterText: text });
  }
  add(event: MatChipInputEvent): void {
    const value = event.value;
    const column = this.filterCol.nativeElement.value;
    if (column === 'search') {
      this.searchKSATList(value);
    }
    else {
      try {
        this.filterKSATList(column.trim(), value.trim());
      } catch {
        console.log('Error: could not add filter');
      }

    }

    // Reset the input value
    this.filterInput.nativeElement.value = '';
  }

  addBySelection(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.viewValue;
    const column = this.filterCol.nativeElement.value;
    this.filterKSATList(column.trim(), value.trim());
    this.filterInput.nativeElement.value = '';
  }
}
