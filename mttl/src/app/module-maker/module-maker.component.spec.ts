import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleMakerComponent } from './module-maker.component';


describe('ModuleMakerComponent', () => {
  let component: ModuleMakerComponent;
  let fixture: ComponentFixture<ModuleMakerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModuleMakerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleMakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
