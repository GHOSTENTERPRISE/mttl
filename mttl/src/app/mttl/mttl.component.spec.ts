import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MttlComponent } from './mttl.component';

describe('MttlComponent', () => {
  let component: MttlComponent;
  let fixture: ComponentFixture<MttlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MttlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MttlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
