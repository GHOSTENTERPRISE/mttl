import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewKsatPopupComponent } from './new-ksat-popup.component';

describe('NewKsatPopupComponent', () => {
  let component: NewKsatPopupComponent;
  let fixture: ComponentFixture<NewKsatPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewKsatPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewKsatPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
