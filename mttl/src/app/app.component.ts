import { Component, OnInit, OnChanges, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';

import * as introJs from 'intro.js/intro.js';

import * as halfmoon from 'halfmoon';

import { faMoon, faAngleDown, faAngleRight, faCode } from '@fortawesome/free-solid-svg-icons';

import { faGitlab } from '@fortawesome/free-brands-svg-icons';

import { MatDialog } from '@angular/material/dialog';

import { ContactUsComponent } from './contact-us/contact-us-popup.component';

//Need this to get oauth info
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

// this is for redirect URI - maybe use express?
import { environment } from '../environments/environment';


import { ApiService } from './api.service';

// profile service for oauth with gitlab
import { ProfileService } from './profile.service';



@Component({
  selector: 'app-root',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ProfileService]
})
export class AppComponent implements OnInit, OnChanges {
  down = faAngleDown;
  right = faAngleRight;
  moon = faMoon;
  gitlab = faGitlab;
  codeIcon = faCode;

  introJS = introJs();
  // for profile validation
  accessToken;
  state: string;
  code: string;
  cookieURL: string;
  profileCookieValue: string;
  client_id = 'f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1';
  part_two: string;
  redirect_uri: string;
  profileName;
  profileInfo;
  isLoggedIn = false;
  redirected = false;
  profileLoaded = false;
  hasProfileCookie;
  isOK;
  //for Profile cookie
  chars = [...'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'];

  private cookieValue: string;
  constructor(
    private apiService: ApiService,
    private cookieService: CookieService,
    private dialog: MatDialog,
    private http: HttpClient,
    private route: ActivatedRoute,
    // private changeDetect: ChangeDetectorRef,
    private profileService: ProfileService) {
    this.redirect_uri = environment.REDIRECT_URI;
    // this.profileName = this.profileService.getProfileName();
    // this.profileInfo = this.profileService.getProfileInfo();
  }



  async getGitlab(code, state) {
    await this.profileService.getTokenWithCode(code);
  }

  async getProfileStatusfromProfileService() {
    const profileLoaded = await this.profileService.getHasProfile();
    this.loadProfile();
    return profileLoaded;
  }

  async getHasProfileCookie() {
    this.hasProfileCookie = await this.profileService.getHasProfileCookie();
  }

  async loadProfile() {
    try {
      await this.profileService.getProfileWithCookie();
      this.getProfileInfoFromService();
      this.getProfileNamefromService();
    } catch {

    }
    if (this.profileName !== undefined) {
      this.profileLoaded = true;
      this.isLoggedIn = true;
    }

  }

  async getProfileInfoFromService() {
    this.profileInfo = await this.profileService.getProfileInfo();
    if (this.profileInfo !== undefined) {
      this.profileLoaded = true;
    }
    return this.profileInfo;
  }

  async getProfileNamefromService() {
    this.profileName = await this.profileService.getProfileName();
    if (this.profileName !== undefined) {
      this.profileLoaded = true;
    }
    return this.profileName;
  }

  //  async getOKness(){
  //   this.isOK = await this.profileService.getOKWithCookie();
  //   return this.isOK;
  // }


  async sayGoodbye() {
    console.log('saying goodbye');
    if (localStorage.getItem('refreshed')) {
      localStorage.removeItem('refreshed');
      console.log('unrefreshed');
    }
    if (localStorage.getItem('logged out')) {
      localStorage.removeItem('logged out');
    }
    if (localStorage.getItem('FF-refresh')) {
      localStorage.removeItem('FF-refresh');
    }
    await this.profileService.removeProfileWithCookie().then(res => {
      // setTimeout(window.location.reload.bind(window.location), 1200);
      console.log('app component - about to window reload after profile service remove cookie');
       window.setTimeout('window.location.reload()', 90);
       console.log('app component - reloaded after profile service remove cookie');
    }
    );
    // setTimeout(window.location.reload.bind(window.location), 1250);
    // window.setTimeout('window.location.reload()', 1200);
    console.log('app component - about to location reload after profile service remove cookie');
   location.reload();
   console.log('app component - location reloaded after profile service remove cookie');
  }


  async sayFarewell() {
    console.log('saying farewell');
    if (localStorage.getItem('refreshed')) {
      localStorage.removeItem('refreshed');
      console.log('unrefreshed');
    }
    if (localStorage.getItem('logged out')) {
      localStorage.removeItem('logged out');
    }
    if (localStorage.getItem('FF-refresh')) {
      localStorage.removeItem('FF-refresh');
    }


    await this.profileService.removeAndRevokeProfileWithCookie();
    // note  - this does not have the timeout

  }

  setProfileCookie(): void {
    this.profileService.setProfileCookie();
  }

  setProfileName(name) {
    this.profileName = name;
  }

  reloadCss()
  {
      const links = document.getElementsByTagName('link');
      for (const cl in links)
      {if (links.hasOwnProperty(cl)){
      {
          const link = links[cl];
          if (link.rel === 'stylesheet')
              {link.href += '';}
      }}}
  }

  toggleDarkMode(): void {
    halfmoon.toggleDarkMode();
    this.reloadCss();
    // this.changeDetect.markForCheck();
    // this.changeDetect.detectChanges();
    location.reload();
  }
  contactUs(): void {
    const dialogRef = this.dialog.open(ContactUsComponent, {
      height: '400px',
      width: '390px',
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  ngOnInit(): void {

    this.introJS.setOptions({
      steps: [
        {
          element: '#step1',
          intro: 'Welcome to 90th Cyber Operations Squadron\'s \nMaster Training Task List',
          position: 'bottom'
        },
        {
          element: '#step2',
          intro: 'Clicking here returns to the landing page',
          position: 'right'
        },
        {
          element: '#step3',
          intro: 'MTTL - is the interactive database listing work role requirements by Knowledge, Skills, Abilities, and Tasks (KSATs)',
          position: 'top'
        },
        {
          element: '#step4',
          intro: ('Links to this website\'s official documentation and evaluation related resources, including recommended study materials'
            + 'for 90th work roles, evaluation scheduling, and information about work role requirements.'),
          position: 'right'
        },
        {
          element: '#step5',
          intro: 'This is a Kumu network graph of the relationships among KSATs',
          position: 'right'
        },
        {
          element: '#step6',
          intro: 'CYT can recommend a variety of training resources. <br> CYV can help with any evaluation needs.',
          position: 'top'
        },
        {
          element: '#step8',
          intro: 'Training resources provided by 90 COS, 39 IOS, and others.',
          position: 'right'
        },
        {
          element: '#step9',
          intro: 'A training road map illustrating the training pathways by work role.',
          position: 'right'
        },
        {
          element: '#step10',
          intro: 'Analysis of training and evaluation coverage organized by KSAT and work role.',
          position: 'right'
        },
        {
          element: '#step11',
          intro: 'Please contact CYT with any questions.',
          position: 'right'
        }
      ],
      showProgress: true
    });

    this.cookieValue = this.cookieService.get('mttl');
    if (this.cookieValue === '') {
      if (window.location.pathname === '/') {
        this.introJS.start().oncomplete(() => {
          window.location.href = 'mttl?multipage=true';
        });
        this.cookieService.set('mttl', '1', 365);
      }
    }

    // if (localStorage.getItem('refreshed')) {
    //   localStorage.removeItem('refreshed');
    //   console.log('unrefreshed');}
    //   if (localStorage.getItem('logged out')) {
    //     localStorage.removeItem('logged out');
    //     location.reload();
    //   }

    this.code = this.route.snapshot.queryParamMap.get('code');

    this.getHasProfileCookie();
    if (this.hasProfileCookie) {
      this.getProfileStatusfromProfileService();
      if (localStorage.getItem('refreshed')) {
        localStorage.removeItem('refreshed');
        console.log('unrefreshed');
      }
    }


    if (this.getHasProfileCookie() && !this.profileLoaded) {
      this.loadProfile();
    }

    if (this.profileLoaded && this.profileName === undefined) {
      this.getProfileInfoFromService();
    }


      this.profileService.profileNameChange.subscribe(value => {
        this.profileName = value;
        if (this.profileName !== undefined && this.profileName !== '') {
          this.isLoggedIn = true;
          // this is not necessary except for Firefox
          if (!localStorage.getItem('FF-refresh')) {
            localStorage.setItem('FF-refresh', 'no reload');
            console.log('app component - about to reload after profile service name change - FF');
            setTimeout(window.location.reload.bind(window.location), 360);
            console.log('app component - reloaded after profile service name change - FF');
          }
        }
      });



      this.profileService.profileInfoChange.subscribe(value => {
        this.profileInfo = value;
      }
      );


    // this.profileService.getOKWithCookie();
    // this.profileService.isOKchange.subscribe(value =>
    //   {
    //     this.isOK = value;
    //   }
    // );

    // if ((!this.profileLoaded) && this.profileCookieValue !== '') {
    //     this.profileService.getProfileWithCookie()
    // }
  }

  ngOnChanges(): void {
    this.cookieValue = this.cookieService.get('mttl');
    if (this.cookieValue === '') {
      if (window.location.pathname === '/') {
        this.introJS.start().oncomplete(() => {
          window.location.href = 'mttl?multipage=true';
        });
        this.cookieService.set('mttl', '1', 365);
      }
    }

    this.code = this.route.snapshot.queryParamMap.get('code');
    // if (this.code !== '' && this.code !== undefined) {
    //   this.profileService.getTokenWithCode(this.code);
    // }

    this.getHasProfileCookie();
    if (!this.profileLoaded && this.hasProfileCookie) {
      this.getProfileStatusfromProfileService();
    }

    if ((!this.profileLoaded) && this.profileCookieValue !== '') {
      this.profileService.getProfileWithCookie();
    }
    //this.getHasProfileCookie();

    if (this.hasProfileCookie) {
      this.getProfileStatusfromProfileService();
    }


    if (this.getHasProfileCookie() && !this.profileLoaded) {
      this.loadProfile();
    }

    if (this.profileLoaded && this.profileName === undefined) {
      this.getProfileInfoFromService();
    }


      this.profileService.profileInfoChange.subscribe(data => {
        const profileInfo = Object.keys(data).map(key => data[key]);
        this.profileName = profileInfo[3][0].name;
        const type = typeof this.profileName;
        if (typeof this.profileName === 'undefined' || this.profileName === null) {
          this.isLoggedIn = false;
        } else {
          this.isLoggedIn = true;
          this.profileName = this.profileService.getProfileName();
          this.profileInfo = this.profileService.getProfileInfo();
        }
        return this.profileInfo;
      });
    }


}

