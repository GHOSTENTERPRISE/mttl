/* ANGULAR IMPORTS */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';

/* ANGULAR MATERIAL */
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

/* ANGULAR FORMS */
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Routing */
import { AppRoutingModule } from './app-routing.module';

/* THIRD PARTY */
import { NgxPaginationModule } from 'ngx-pagination';
import { CookieService } from 'ngx-cookie-service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CarouselModule } from 'ngx-bootstrap/carousel';

/* SERVICES */
import { ApiService } from './api.service';
// profile service for oauth with gitlab
import { ProfileService } from './profile.service';


/* COMPONENTS */
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MttlComponent } from './mttl/mttl.component';
import { KsatLinkPromptComponent } from './ksat-link-prompt/ksat-link-prompt.component';
import { NewKsatPopupComponent } from './new-ksat-popup/new-ksat-popup.component';
import { ContactUsComponent } from './contact-us/contact-us-popup.component';

/* PIPES */
import { FilterByArrayPipe } from './pipes/filter-by-array.pipe';
import { FilterMTTLPipe } from './pipes/filter-mttl.pipe';
import { CoverageCalculatorPipe } from './pipes/coverage-calculator.pipe';
import { OrderBy } from './pipes/order-by.pipe';
import { AutocompletePipe } from './pipes/autocomplete.pipe';
import { FAQSearchPipe } from './pipes/faqsearch.pipe';
import { SearchAndFilterPipe } from './pipes/search-and-filter.pipe';

/* PAGE NOT FOUND */
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

/*Export Component*/
import { ExportDataService } from './export-data.service';

/* Metrics Components */
import { MetricsComponent } from './metrics/metrics.component';
import { WrMetricsComponent } from './wr-metrics/wr-metrics.component';
import { AllMetricsComponent } from './all-metrics/all-metrics.component';
import { RoadmapComponent } from './roadmap/roadmap.component';
import { FAQComponent } from './faq/faq.component';
import { ModuleViewerComponent } from './module-viewer/module-viewer.component';
import { ModuleMakerComponent } from './module-maker/module-maker.component';
import { RoadmapOverviewComponent } from './roadmap-overview/roadmap-overview.component';
import { WorkroleRoadmapComponent } from './workrole-roadmap/workrole-roadmap.component';
import { IdfKsatsComponent } from './idf-ksats/idf-ksats.component';
import { MiscTablesComponent } from './misc-tables/misc-tables.component';
import { NoWrMetricsComponent } from './no-wr-metrics/no-wr-metrics.component';
import { BugReportComponent } from './bug-report/bug-report.component';
import { ModifyKsatDialogComponent } from './modify-ksat-dialog/modify-ksat-dialog.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { SelectColumnsComponent } from './select-columns/select-columns.component';

// material tables
import { MatTableModule } from '@angular/material/table';



@NgModule({
  declarations: [
    AppComponent,
    FilterByArrayPipe,
    FilterMTTLPipe,
    SearchAndFilterPipe,
    CoverageCalculatorPipe,
    OrderBy,
    MttlComponent,
    PageNotFoundComponent,
    KsatLinkPromptComponent,
    NewKsatPopupComponent,
    MetricsComponent,
    WrMetricsComponent,
    AllMetricsComponent,
    HomepageComponent,
    RoadmapComponent,
    ContactUsComponent,
    FAQComponent,
    AutocompletePipe,
    FAQSearchPipe,
    ModuleViewerComponent,
    ModuleMakerComponent,
    RoadmapOverviewComponent,
    WorkroleRoadmapComponent,
    IdfKsatsComponent,
    MiscTablesComponent,
    NoWrMetricsComponent,
    BugReportComponent,
    ModifyKsatDialogComponent,
    DisclaimerComponent,
    SelectColumnsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatDividerModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    FlexLayoutModule,
    FontAwesomeModule,
    MatChipsModule,
    MatTooltipModule,
    CarouselModule.forRoot(),
    DragDropModule
  ],
  providers: [ApiService,
    CookieService,
    ProfileService,
    ExportDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
