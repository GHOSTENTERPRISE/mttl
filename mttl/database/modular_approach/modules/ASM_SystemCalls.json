{
  "ASM_SystemCalls": {
    "uniq_id": "IDF_ASM_system_calls",
    "uniq_name": "ASM_SystemCalls",
    "description": "This module is primarily only informative aiming to help students understand the purpose of system calls and interrupts,  different processor modes in Assembly, how to access files in Assembly",
    "objective": "Understand the purpose of system calls and interrupts;\nUnderstand and access different processor modes in Assembly;\nAccess files in Assembly;\nDevelop familiarity with  Assembly debugging using WinDBG",
    "content": [
      {
        "description": "Calling conventions, name mangling, etc. for Microsoft and Sys V in Assembly",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Calls.html"
      },
      {
        "description": "Informational Only: System Calls in Assembly",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html"
      },
      {
        "description": "Understanding WinDBG",
        "ref": "https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg"
      }
    ],
    "performance": [],
    "required_tasks": [],
    "required_abilities": [],
    "required_skills": [
      {
        "ksat_id": "S0120",
        "completes": false,
        "description": "Use interrupts to execute OS system calls."
      },
      {
        "ksat_id": "S0122",
        "completes": false,
        "description": "Invoke system calls."
      },
      {
        "ksat_id": "S0132",
        "completes": false,
        "description": "Write Assembly code for different processor modes."
      },
      {
        "ksat_id": "S0124",
        "completes": false,
        "description": "Implement file handling."
      }
    ],
    "required_knowledge": [
      {
        "ksat_id": "K0241",
        "completes": true,
        "description": "With references and required resources, describe the purpose and use of system interrupts."
      },
      {
        "ksat_id": "K0242",
        "completes": true,
        "description": "System interrupts"
      },
      {
        "ksat_id": "K0243",
        "completes": true,
        "description": "With references and required resources, describe the purpose, use, and terms of operating system calls (syscall) to request kernel mode services."
      },
      {
        "ksat_id": "K0814",
        "completes": true,
        "description": "Describe common interrupts and how to use them."
      },
      {
        "ksat_id": "K0816",
        "completes": true,
        "description": "Describe how to invoke system calls."
      },
      {
        "ksat_id": "K0821",
        "completes": true,
        "description": "Understand different processor modes."
      },
      {
        "ksat_id": "K0168",
        "completes": true,
        "description": "Understand operating system memory hierarchy"
      },
      {
        "ksat_id": "K0152",
        "completes": true,
        "description": "Differentiate between kernel and user-mode memory"
      },
      {
        "ksat_id": "K0818",
        "completes": false,
        "description": "Understand how to implement file handling."
      },
      {
        "ksat_id": "K0161",
        "completes": false,
        "description": "Describe Harvard Architecture"
      },
      {
        "ksat_id": "K0160",
        "completes": false,
        "description": "Describe Von Neumann Architecture"
      },
      {
        "ksat_id": "K0820",
        "completes": false,
        "description": "Describe how to use a WinDBG debugger to identify errors in a program."
      }
    ],
    "resources": [
      {
        "title": "Nebbett, G. (2000). Windows NT/2000 native API reference. Sams Publishing",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "Nebbett, G. (2000). Windows NT/2000 native API reference. Sams Publishing"
      },
      {
        "title": "https://asmtutor.com/#lesson22",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://asmtutor.com/#lesson22"
      },
      {
        "title": "https://www.cs.uaf.edu/2016/fall/cs301/lecture/11_04_syscall.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.cs.uaf.edu/2016/fall/cs301/lecture/11_04_syscall.html"
      },
      {
        "title": "https://wiki.osdev.org/Virtual_8086_Mode",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://wiki.osdev.org/Virtual_8086_Mode"
      },
      {
        "title": "https://riptutorial.com/x86/example/12672/real-mode",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://riptutorial.com/x86/example/12672/real-mode"
      },
      {
        "title": "https://wiki.osdev.org/Real_Mode",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://wiki.osdev.org/Real_Mode"
      },
      {
        "title": "https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux#Via_interrupt",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux#Via_interrupt"
      },
      {
        "title": "https://asmtutor.com/#lesson1",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://asmtutor.com/#lesson1"
      },
      {
        "title": "https://www.tutorialspoint.com/assembly_programming/assembly_basic_syntax.htm",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.tutorialspoint.com/assembly_programming/assembly_basic_syntax.htm"
      },
      {
        "title": "https://www.codeproject.com/Articles/45788/The-Real-Protected-Long-mode-assembly-tutorial-for",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.codeproject.com/Articles/45788/The-Real-Protected-Long-mode-assembly-tutorial-for"
      },
      {
        "title": "https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm"
      },
      {
        "title": "http://faculty.nps.edu/cseagle/assembly/sys_call.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "http://faculty.nps.edu/cseagle/assembly/sys_call.html"
      },
      {
        "title": "https://www.tutorialspoint.com/assembly_programming/assembly_file_management.htm",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.tutorialspoint.com/assembly_programming/assembly_file_management.htm"
      },
      {
        "title": "https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://en.wikibooks.org/wiki/X86_Assembly/Interfacing_with_Linux"
      },
      {
        "title": "https://wiki.osdev.org/System_Management_Mode",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://wiki.osdev.org/System_Management_Mode"
      },
      {
        "title": "https://wiki.osdev.org/Protected_Mode",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://wiki.osdev.org/Protected_Mode"
      },
      {
        "title": "https://software.intel.com/content/www/us/en/develop/articles/intel-sdm.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://software.intel.com/content/www/us/en/develop/articles/intel-sdm.html"
      },
      {
        "title": "http://www.c-jump.com/CIS77/ASM/Memory/lecture.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "http://www.c-jump.com/CIS77/ASM/Memory/lecture.html"
      },
      {
        "title": "https://www.researchgate.net/publication/241643659_Using_CPU_System_Management_Mode_to_Circumvent_Operating_System_Security_Functions",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.researchgate.net/publication/241643659_Using_CPU_System_Management_Mode_to_Circumvent_Operating_System_Security_Functions"
      },
      {
        "title": "https://resources.infosecinstitute.com/calling-ntdll-functions-directly/#gref",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://resources.infosecinstitute.com/calling-ntdll-functions-directly/#gref"
      },
      {
        "title": "https://wiki.osdev.org/Security#Rings",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://wiki.osdev.org/Security#Rings"
      },
      {
        "title": "https://j00ru.vexillium.org/syscalls/nt/64/",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://j00ru.vexillium.org/syscalls/nt/64/"
      },
      {
        "title": "https://stackoverflow.com/questions/29440225/in-linux-x86-64-are-syscalls-and-int-0x80-related",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://stackoverflow.com/questions/29440225/in-linux-x86-64-are-syscalls-and-int-0x80-related"
      },
      {
        "title": "https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/"
      }
    ],
    "comments": "generated by rellink to module script",
    "assessment": [
      {
        "description": "Create a 'hello world' program in Assembly, that's writes to a file, and explain how it uses system calls.",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html"
      },
      {
        "description": "Provide a brief written description (in your own words) explaining real, protected, and system management mode in Assembly.",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_SystemCalls/SystemCalls.html"
      }
    ]
  }
}
