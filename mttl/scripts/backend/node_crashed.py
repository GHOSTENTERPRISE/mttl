#!/usr/bin/env python3
import os
import requests
import datetime
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504]
    )
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")
project_id = 18738190
crash_time = datetime.datetime.now()
crash_info = ""

with open('log.txt', 'r') as log:
    crash_info = log.read()

crash_info = crash_info.split('\n')
crash_info = crash_info[-15:]
crash_info = "\n".join(crash_info)

data = {
    'title': 'Express Server Crashed: '
    + crash_time.strftime("%m/%d/%y %H:%M:%S"),
    'description': "The most recent 15 lines of stdout and stderr read: " + """

```
    """ + crash_info.replace("`", "") + """
```

For more information, please check the logfile at /app/crash_log.txt"""
    + " in the production pod" + """

/label ~BUG ~"office::CYT" ~"CYT::MTTL"
/milestone %"CYT Backlog"
    """
}

res = gl.post(
    gitlab_api+'/'+str(project_id)+'/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token}
)

print(res.json())
