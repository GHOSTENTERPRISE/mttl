#!/usr/bin/env python3
import requests
import sys
from urllib.parse import unquote
import datetime
import os
import gitlab_email


from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(total=5,
                backoff_factor=1,
                status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")
username = unquote(sys.argv[1])
jsondata = unquote(sys.argv[2])

project_id = 18738190
today = datetime.datetime.now()
email_blurb = gitlab_email.get_email_blurb(username)

data = {
    'title': 'Module submitted from Form - '
    + today.strftime('%m')
    + '/'
    + today.strftime('%d')
    + '/'
    + today.strftime('%y'),
      'description': """
# Submitter Info:
Submitter: @""" + username + """

## JSON:
""" + jsondata + """

""" + email_blurb + """
/label ~customer ~"office::CYT" ~"backlog::idea" ~USER_SUBMISSIONS ~"CYT::MTTL"
/milestone % "CYT Backlog"
"""
}

print(data)
res = gl.post(
    gitlab_api + '/' + str(project_id) + '/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token})
