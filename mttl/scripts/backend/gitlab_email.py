import os
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504]
    )
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/users'
private_token = os.getenv("TOKEN")
project_id = 18738190


def get_email_from_username(username):
    user_id = get_id_from_username(username)
    if not user_id:
        return ''

    res = gl.get(
        gitlab_api + '/' + str(user_id),
        headers={'PRIVATE-TOKEN': private_token}
    )

    try:
        return res.json()['public_email']
    except KeyError:
        return ''


def get_id_from_username(username):
    res = gl.get(
        gitlab_api+'?username=' + username,
        headers={'PRIVATE-TOKEN': private_token}
    )
    try:
        return res.json()[0]['id']
    except (IndexError, KeyError):
        return ''


def get_email_blurb(username):
    email = get_email_from_username(username)
    if (email):
        blurb = "User's Public Email Address: " + email
    else:
        blurb = ''
    return blurb
