#! /usr/bin/env python3

import json
import sys

file = sys.argv[1]

# Open JSON to be sorted
f = open(file, "r")
data = json.load(f)

# Pull only data needed to be sorted into new list
lines = data['TTL']

# Sort using ksat_id
lines = sorted(lines, key=lambda var: (var.get('ksat_id')))
temp = data['TTL']
data['TTL'] = lines

# Create new JSON file to output
new_file = open(file, "w")
new_file.write(json.dumps(data, indent=4, sort_keys=False))

# Close files
new_file.close()
f.close()
