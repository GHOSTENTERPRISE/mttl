#!/usr/bin/env python3
import requests
import sys
from urllib.parse import unquote
import datetime
import os
import gitlab_email

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(total=5,
                backoff_factor=1,
                status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")
name = unquote(sys.argv[1])
topic = unquote(sys.argv[2])
workrole = unquote(sys.argv[3])
owner = unquote(sys.argv[4])
source = unquote(sys.argv[5])
parents = unquote(sys.argv[6])
children = unquote(sys.argv[6])
training_link = unquote(sys.argv[8]).replace('\n', '    \n')
training_ref = unquote(sys.argv[9]).replace('\n', '    \n')
evals = unquote(sys.argv[10]).replace('\n', '    \n')
desc = unquote(sys.argv[11]).replace('\n', '    \n')
username = unquote(sys.argv[12])
project_id = 18738190
today = datetime.datetime.now()
email_blurb = gitlab_email.get_email_blurb(username)

data = {
    'confidential': False,
    'title': 'KSAT Submission - '
    + today.strftime('%m')
    + '/'
    + today.strftime('%d')
    + '/'
    + today.strftime('%y'),
    'description': """
## Submitter Info:
#### Submitter's Name:
""" + name + """

#### Username:
@""" + username + """

""" + email_blurb + """

## KSAT Info:

#### Topic:
""" + topic + """

#### Work-Role:
""" + workrole + """

#### KSAT Description:
""" + desc + """

## Source Info:
#### Requirement Owner:
""" + owner + """

#### Requirement Source:
""" + source + """

## Related KSATS:
#### Parents:
""" + parents + """

#### Children:

""" + children + """

## Training:
#### Training Link:

""" + training_link + """

#### Training Reference:

""" + training_ref + """

## Evaluations
#### Evals:

""" + evals + """

/label ~customer ~"office::CYT" ~"backlog::idea" ~USER_SUBMISSIONS ~"CYT::MTTL"
/label ~"mttl::ksat"
/milestone %"CYT Backlog"
"""
}

# print("sending ", data)
res = gl.post(
    gitlab_api + '/' + str(project_id) + '/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token})

# sending a string for the web_url
json_response = res.json()
try:
    json_response["web_url"]
except KeyError:
    json_response = None

if json_response is not None:
    print(json_response["web_url"])
else:
    print("Spam response")
