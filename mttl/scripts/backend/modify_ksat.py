#!/usr/bin/env python3
import os
import requests
import sys
import gitlab_email

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(total=5,
                backoff_factor=1,
                status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")
ksat_id = sys.argv[1]
desc = sys.argv[2]
username = sys.argv[3]
project_id = 18738190
email_blurb = gitlab_email.get_email_blurb(username)

data = {
    'title': 'Modify KSAT: ' + ksat_id,
    'description': "@" + username + " Submitted: " + desc + """

""" + email_blurb + """
/label ~customer ~"office::CYT" ~"backlog::idea" ~USER_SUBMISSIONS ~"CYT::MTTL"
/label ~"mttl::ksat"
/milestone %"CYT Backlog" """
}

res = gl.post(
    gitlab_api + '/' + str(project_id) + '/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token})

# sending a string for the web_url
json_response = res.json()
try:
    json_response["web_url"]
except KeyError:
    json_response = None

if json_response is not None:
    print(json_response["web_url"])
else:
    print("Spam response")
