#! /usr/bin/env python3

import json
import sys
import csv


if len(sys.argv) > 2:
    wr_file = sys.argv[1]
    oam_file = sys.argv[2]
else:
    print("Specify work role file, e.g. CCD.json,\
           then oam file, e.g., ccd_oam.csv")
    print("Nothing to do. Exiting.")
    sys.exit()

# Open work role file
wr_f = open(wr_file, "r")
data = json.load(wr_f)

# Pull only data needed to be sorted into new list
lines = data['TTL']

# Sort using ksat_id
lines = sorted(lines, key=lambda var: (var.get('ksat_id')))
temp = data['TTL']
data['TTL'] = lines

with open(oam_file, 'rt') as csvfile:
    reader = csv.reader(csvfile, quotechar='"', skipinitialspace=True)
    oams = list(reader)

orphaned_expectations = []
for oam in oams:
    ksat_for_oam_exists = False
    for wr_ksat in data['TTL']:
        # print("oam: {}, wr_ksat: {}".format(oam, wr_ksat))
        if oam[2] == wr_ksat['ksat_id']:
            wr_ksat['overarching_milestone'] = oam[1]
            ksat_for_oam_exists = True
    if not ksat_for_oam_exists:
        orphaned_expectations.append(oam)
        print("expected ksat for OAM not found \
               in work role file for {}".format(oam))

orphaned_expectations_filename = "orphaned_expectations_" + wr_file
if len(orphaned_expectations) < 1:
    print("All expected OAM ksats\
           mapped to work role")
else:
    with open(orphaned_expectations_filename,
         "w") as orphaned_expectations_file:
        orphaned_expectations_file.write(json.dumps(
            orphaned_expectations, indent=4, sort_keys=False))


unmapped_ksats = []
for wr_ksat in data['TTL']:
    if 'overarching_milestone' not in wr_ksat:
        print("overarching milestone missing for: ", wr_ksat)
        unmapped_ksats.append(wr_ksat)

if len(unmapped_ksats) < 1:
    print("All KSATs identified as mapped to work role")
else:
    unmapped_filename = "unmapped_" + wr_file
    with open(unmapped_filename, "w") as unmapped_file:
        unmapped_file.write(json.dumps(
            unmapped_ksats, indent=4, sort_keys=False))


new_filename = "oam" + wr_file
with open(new_filename, "w") as new_file:
    new_file.write(json.dumps(data, indent=4, sort_keys=False))
