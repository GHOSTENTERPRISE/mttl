#! /bin/bash 
 [[ ! "$PWD" =~ postgres ]] && cd mttl/scripts/postgres/
echo "This shell script calls the Python script to import Autocomplete to Postgres"
/usr/bin/python3 ./import_autocomplete_to_local_postgres.py 

exit 0 
