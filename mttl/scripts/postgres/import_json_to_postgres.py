#! /usr/bin/python3
# https://stackoverflow.com/questions/54778762/how-to-create-a-new-table-in-postgresql-from-a-json-file-using-python-and-psyco
# sudo pip3 install psycopg2-binary

import psycopg2
import json


from sqlalchemy import create_engine
from pandas import json_normalize


with open('MTTL.min.json') as data_file:
    d = json.load(data_file)


def create_table():
    conn = psycopg2.connect("dbname='mttl' user='mttl_user' \
           host='postgres' port='5432' ")
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS mttl (_id TEXT, ksat_id TEXT, \
                 parent TEXT, description TEXT, topic TEXT, \
                 requirement_src TEXT, \
                 requirement_owner TEXT, comments TEXT, updated_on TEXT, \
                 children TEXT, training_links TEXT, eval_links TEXT, \
                 work_roles TEXT [], work_roles_specializations TEXT, \
                 requirement_src_id TEXT, created_on TEXT, \
                 specializations TEXT, refs TEXT)")
    conn.commit()
    conn.close()


def remove_prof_data(old_data):
    n = old_data.copy()
    n.pop('proficiency_data', None)
    n.pop('training', None)
    n.pop('eval', None)
    return n


create_table()
print('table created')
d = map(remove_prof_data, d)
df = json_normalize(d)

engine = create_engine(
    "postgresql+psycopg2://mttl_user@postgres:5432/mttl")
df.to_sql("mttl", engine, index=False, if_exists='append')
print("Done")
