#! /usr/bin/python3
# https://stackoverflow.com/questions/54778762/how-to-create-a-new-table-in-postgresql-from-a-json-file-using-python-and-psyco
# sudo pip3 install psycopg2-binary

import psycopg2
import json


from sqlalchemy import create_engine
from pandas import json_normalize


with open('autocomplete.min.json') as data_file:
    d = json.load(data_file)


def create_table():
    conn = psycopg2.connect("dbname='mttl' user='mttl_user' \
           host='postgres' port='5432' ")
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS autocomplete (ksat_id TEXT, \
                 topic TEXT, \
                 requirement_src TEXT, \
                 requirement_owner TEXT, \
                 work_role TEXT)")
    conn.commit()
    conn.close()


create_table()
print('table created')
df = json_normalize(d)

engine = create_engine(
    "postgresql+psycopg2://mttl_user@postgres:5432/mttl")
print(df.columns)
df.to_sql("autocomplete", engine, index=False, if_exists='append')
print("Done")
