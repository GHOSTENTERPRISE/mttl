#!/usr/bin/python3

import os
import unittest
import subprocess
import pymongo
import json
import random

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


class TestMethods(unittest.TestCase):

    def test_insert_rel_link_mapping(self):
        num_items = 1
        test_rels = list(rls.aggregate([
            {'$match': {'map_for': 'training'}},
            {'$sample': {'size': num_items}}
        ]))
        test_ksats = list(reqs.aggregate([
            {'$sample': {'size': num_items}}
        ]))

        for i, test_rel in enumerate(test_rels):
            test_ksat = test_ksats[i]
            cmd = "python3 mttl.py modify rel-link -i "\
                  f"{str(test_rel['_id'])} {test_ksat['ksat_id']} A test"
            subprocess.run(cmd.split(),
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
            modified_rel_count = rls.count_documents(
                {
                    "_id": test_rel['_id'],
                    "KSATs.ksat_id": test_ksat['_id'],
                    'KSATs.url': 'test'})
            # if modified count == 0 nothing was modified
            self.assertEqual(modified_rel_count, 1)

    def test_delete_mapping(self):
        test_rel = list(rls.aggregate([
            {'$match': {
                'KSATs': {'$not': {'$size': 0}},
                'map_for': 'training'}},
            {'$sample': {'size': 1}}
        ]))[0]

        find_qry = json.dumps(
            {'_id': str(test_rel['_id'])},
            separators=(',', ':')
        )
        cmd = f"python3 mttl.py modify rel-link -d {find_qry}".split()
        for mapping in test_rel['KSATs']:
            cmd.append(reqs.find_one({'_id': mapping['ksat_id']})['ksat_id'])
            subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        modified_rel = rls.find_one({"_id": test_rel['_id']})
        # if modified KSATs len == 0 everything removed
        self.assertEqual(len(modified_rel['KSATs']), 0)

    def test_modify_update_ksat_id(self):
        num_items = 1
        test_ksats = list(reqs.aggregate([
            {'$sample': {'size': num_items}}
        ]))

        old_ksats = [ksat['ksat_id'] for ksat in test_ksats]
        new_ksats = []
        while len(new_ksats) < num_items:
            rand_ksat_id = random.randrange(2000, 9999)
            new_ksat = f'K{rand_ksat_id:4}'
            if reqs.count_documents({'ksat_id': new_ksat}) == 0:
                new_ksats.append(new_ksat)

        cmd = 'python3 mttl.py modify ksat -k'.split()
        for i, old_ksat in enumerate(test_ksats):
            cmd.append(f'{old_ksat["ksat_id"]}:skills')
        subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.assertEqual(
            reqs.count_documents({'ksat_id': {'$in': old_ksats}}), 0)


if __name__ == "__main__":
    unittest.main()
