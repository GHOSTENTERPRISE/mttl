import os
import json
import subprocess
import pymongo

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles


def run_cmd(
    cmd: list,
    shell=False,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    stdin=subprocess.PIPE
):
    proc = subprocess.Popen(cmd, shell=shell)
    proc.communicate()
    return proc.returncode


def get_all_json(root, identifier, avoid_list=[]):
    json_list = []

    for dir_name, subdir_list, file_list in os.walk(root):
        for fname in file_list:
            if identifier in fname and fname not in avoid_list:
                json_list.append(os.path.join(dir_name, fname))
    return json_list


def get_requirement_dict(file_path: str) -> dict:
    '''
    Purpose: Generates a Python dictionary version of a requirement file,
    which is expected to be a JSON file.
    :param file_path: The path to the requirement file to convert
    to a dictionary
    :return: Upon success: json_data = a dict of the requested requirements
             Upon failure:
                raise ValueError = the input param 'file_path' doesn't exist
                raise json.JSONDecodeError, FileNotFoundError,
                    FileExistsError, OSError, IOError, Exception,
                    BaseException = the input param 'file_path'
                    cannot be loaded into a dictionary
                {} = Data Error
    '''
    if not os.path.exists(file_path):
        # Prevent exposing variable name in stack trace
        msg = "Is '{0}' the correct path?.".format(file_path)
        raise ValueError(msg)

    try:
        json_data = json.load(open(file_path, "r"))

    except (
        json.JSONDecodeError,
        FileNotFoundError,
        FileExistsError,
        OSError,
        IOError,
        Exception,
        BaseException
    ) as e:
        raise ValueError("Unable to build requirements: {0}".format(e))

    return json_data


def get_all_requirements(req_files: list) -> dict:
    '''
    Purpose: Generates a single dictionary of all requirements.
    :param req_files: A list of file paths to the requirement files
    :return: Upon success: reqs = a dict of all requirements
             Upon failure:

                raise ValueError = an empty list of requirements
                files is received or a requirement file doesn't exist

                raise json.JSONDecodeError, FileNotFoundError,
                    FileExistsError, OSError, IOError, Exception,
                    BaseException:
                one or more requirements files cannot be loaded
                into a dictionary
                {} = Data Error
    '''
    reqs = {}

    if len(req_files) == 0:
        # In case the file list couldn't be generated
        raise ValueError("Error building requirements: "
                         "input file list was empty.")

    for req_file in req_files:
        try:
            reqs.update(get_requirement_dict(req_file))

        except (
            json.JSONDecodeError,
            FileNotFoundError,
            FileExistsError,
            OSError,
            IOError,
            ValueError,
            Exception,
            BaseException
        ):
            raise

    return reqs


def convert_lists(ksat: dict):
    '''
    Helper function to find all the fields in the dict
    that are lists of str or dict and convert to strings
    '''
    for key in ksat.keys():
        # check if item is list
        if isinstance(ksat[key], list):
            # if list if len 0, don't care
            if len(ksat[key]) == 0:
                ksat[key] = ''
            # if list of str we join it
            elif isinstance(ksat[key][0], str):
                ksat[key] = ', '.join(ksat[key])
            # if list of dict we combine only the _id
            elif isinstance(ksat[key][0], dict):
                tmp_list = []
                if key != 'work-roles':
                    for item in ksat[key]:
                        tmp_list.append(item['_id'])
                    ksat[key] = ', '.join(tmp_list)
                else:
                    for item in ksat[key]:
                        tmp_list.append(item['work-role'])
                    ksat[key] = ', '.join(tmp_list)


def list_roadmap_files(roadmap_dir: str):
    return [
        f"{roadmap_dir}/{f}"
        for f in os.listdir(roadmap_dir)
        if os.path.isfile(f"{roadmap_dir}/{f}")
    ]


def get_work_roles():
    return wrs.distinct("work-role")


def oid_to_id(ksat_id):
    '''
    Purpose:
    To convert ksat_id to oid for mappings
    '''

    ksat_array = list(reqs.find({'ksat_id': ksat_id}))

    if(len(ksat_array) > 1):
        raise ValueError("Error in OID_to_ID: "
                         "Multiple values found!")
    elif(len(ksat_array) < 1):
        raise ValueError("Error in OID_to_ID: "
                         "No values found!")
    oid = ksat_array[0]['_id']

    return oid
