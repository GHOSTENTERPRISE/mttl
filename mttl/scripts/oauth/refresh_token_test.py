#!/usr/bin/python3
import json
import requests
import sys
from urllib.parse import unquote
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))


def get_code(code) -> str:
    res = gl.post("https://gitlab.com/oauth/token", data={
        'client_id':
        '15ec60b994e6770478e23471410c7f9b44c0163ad69933b651daf0e4469b2f85',
        'client_secret':
        '2e60961caa8de246e75ba3ba66bdc5ab699d449151b70f47a23cdac679ba75cf',
        'redirect_uri': 'http://localhost:5000/redirect',
        'grant_type': 'authorization_code',
        'state': 'bob',
        'code': code,
        'scope': 'profile'
    })
    response = res.json()
    print(json.dumps(response, indent=4, sort_keys=True))
    with open('oauth.txt', 'w') as save_info:
        save_info.write(json.dumps(response, indent=4, sort_keys=True))
    return(response['access_token'])


# This is different than in the oauth program
def get_refresh_token(refresh_token, state) -> str:
    # print(type(accessToken))
    res = gl.post("https://gitlab.com/oauth/token", data={
        'client_id':
        '15ec60b994e6770478e23471410c7f9b44c0163ad69933b651daf0e4469b2f85',
        'client_secret':
        '2e60961caa8de246e75ba3ba66bdc5ab699d449151b70f47a23cdac679ba75cf',
        'redirect_uri': 'http://localhost:5000/redirect',
        'grant_type': 'refresh_token',
        'state': state,
        'refresh_token': refresh_token,
        'scope': 'openid'
    })
    response = res.json()
    print(json.dumps(response, indent=4, sort_keys=True))
    return(response)


def get_access_token(response) -> str:
    print(response['access_token'])


def main():
    print("using code to get access tokens")
    refresh_token = unquote(sys.argv[1])

    if not sys.argv[2]:
        print("refresh token and state are needed")
    else:
        state = unquote(sys.argv[1])
        get_refresh_token(refresh_token, state)


main()
