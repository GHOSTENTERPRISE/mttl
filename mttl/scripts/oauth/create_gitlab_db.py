#!/usr/bin/python3
import psycopg2
import json
import os
import sys
from urllib.parse import urlparse


def get_connection_pg() -> psycopg2.extensions.connection:
    # print("getting connection")
    try:
        pgconn = os.getenv('PGCONN')
        result = urlparse(pgconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)


def get_connection_gl() -> psycopg2.extensions.connection:
    # print("getting connection")
    try:
        glconn = os.getenv('GLCONN')
        result = urlparse(glconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)


def check_for_gitlab_db() -> bool:
    conn = None
    conn = get_connection_pg()
    if conn is not None:
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute("SELECT datname FROM pg_database;")
        list_database = cur.fetchall()
        if ("gitlab",) in list_database:
            conn.close()
            return(True)
        else:
            conn.close()
            return(False)


def create_gitlab_db():
    # print("Creating Gitlab DB")
    conn = None
    conn = get_connection_pg()

    if conn is not None:
        conn.autocommit = True
        cursor = conn.cursor()
        sql = '''CREATE database gitlab'''
        cursor.execute(sql)
        conn.close()
    else:
        print("Error creating Gitlab DB")


def create_gitlab_table():
    # print("creating Gitlab table")
    conn = get_connection_gl()
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS gitlab ( \
                 cookie TEXT PRIMARY KEY, \
                 access_token TEXT, \
                 created_at TEXT, \
                 refresh_token TEXT, \
                 scope TEXT, \
                 token_type TEXT)")
    conn.commit()
    conn.close()


def create_profile_table():
    # print("creating Profile table")
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        cur = conn.cursor()
        cur.execute("CREATE TABLE IF NOT EXISTS profile ( \
                     cookie TEXT PRIMARY KEY, \
                     groups TEXT[], \
                     name TEXT, \
                     nickname TEXT, \
                     profile TEXT, \
                     sub TEXT)")
    # sub_legacy is being deprecated
        conn.commit()
        conn.close()


def check_for_existing_profile(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM profile WHERE cookie = %s", (cookie_id,))
        profile_exists = curs.fetchone() is not None
        conn.close()
        return profile_exists


def check_for_existing_user(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM gitlab WHERE cookie = %s", (cookie_id,))
        user_exists = curs.fetchone() is not None
        conn.close()
        return user_exists


def get_existing_profile(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM profile WHERE cookie = %s", (cookie_id,))
        profile = curs.fetchone()
        conn.close()
        return profile


def get_existing_user(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM gitlab WHERE cookie = %s", (cookie_id,))
        user = curs.fetchone()
        conn.close()
        return user


def insert_token_values_gitlab(cookie, access_token, created_at,
                               refresh_token, scope, token_type):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        insert_query = "INSERT into gitlab (cookie, access_token, created_at, \
                       refresh_token, scope, token_type) VALUES \
                       (%s,%s,%s,%s,%s,%s) ON CONFLICT ON CONSTRAINT\
                        gitlab_pkey DO NOTHING;"
        insert_data = (cookie, access_token, created_at,
                       refresh_token, scope, token_type)
    # update and insert will fail silently if not applicable
        update_query = "UPDATE gitlab SET access_token=%s, created_at=%s, \
                       refresh_token=%s, scope=%s, \
                       token_type=%s WHERE cookie=%s;"
        update_data = (access_token, created_at, refresh_token,
                       scope, token_type, cookie)
        curs.execute(update_query, update_data)
        curs.execute(insert_query, insert_data)
        conn.commit()
        conn.close()


def insert_profile_gitlab(cookie, profile):
    conn = None
    conn = get_connection_gl()
    # print("This is the profile passed to insert profile ", profile)
    profile = json.loads(profile)
    # print("these are the keys")
    # for key in profile:
    #     print(key)
    if conn is not None:
        curs = conn.cursor()
        insert_query = "INSERT into profile (cookie,  name, nickname,\
                       profile, sub, groups) VALUES \
                       (%s,%s,%s,%s,%s,%s);"
        insert_data = (cookie, profile['name'], profile['nickname'],
                       profile['profile'], profile['sub'], profile['groups'])
    # update and insert will fail silently if not applicable
    # - which is intended
        update_query = "UPDATE profile SET name=%s, nickname=%s, \
                       profile=%s, sub=%s, groups=%s WHERE cookie=%s;"
        update_data = (profile['name'], profile['nickname'],
                       profile['profile'], profile['sub'],
                       profile['groups'], cookie)
        curs.execute(update_query, update_data)
        curs.execute(insert_query, insert_data)
        conn.commit()
        conn.close()


def main():
    print("checking for gitlabDB")
    if not check_for_gitlab_db():
        create_gitlab_db()
    create_gitlab_table()
    create_profile_table()


if __name__ == '__main__':
    main()
