#!/usr/bin/python3

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import requests
import sys


gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))


def get_profile_from_gitlab(access_token):
    profile_url = "https://gitlab.com/oauth/userinfo?access_token="\
        + access_token
    res = gl.get(profile_url)
    response = res.json()
    print("Profile received: ")
    print(json.dumps(response, indent=4, sort_keys=True))
    return(json.dumps(response, indent=4, sort_keys=True))
    """{"sub":"",
        "sub_legacy":"",
        "name":"",
        "nickname":"",
        "profile":"",
        "picture":"",
        "groups":[]
        }"
"""


def main():
    try:
        access_token = (sys.argv[1])
    except Exception:
        sys.exit("Using the CLI, provide AccessToken i.e.\
                  ./get_profile.py <access token>")

    profile = get_profile_from_gitlab(access_token)
    print(json.dumps(profile, indent=4, sort_keys=True))


if __name__ == '__main__':
    main()
