#!/usr/bin/python3

import os
import sys
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from mongo_helpers import find_id_and_modify, find_id_and_insert

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
supps = db.supplementary


def get_proficiency(ksats: list, ksat) -> str:
    proficiency = 'N/A'
    just_ksats = ksats['KSATs']
    for item in just_ksats:
        if item['ksat_id'] == ksat:
            return item['proficiency']
    return proficiency


def add_supp_trn_eval(ksat: dict, key: str, link_name_key: str):
    new_list = []
    ksat_match = str(ksat['ksat_id'])
    supp_mapping_ids = supps.aggregate([
        {
            '$match': {
                'KSATs.ksat_id': ksat_match
            }
        },
        {
          '$project': {
                'name': f'${link_name_key}',
                'url': '$url',
                'proficiency': '$proficiency',
                'ksat_id': ksat_match,
                'KSATs': {
                    '$filter': {
                        'input': '$KSATs',
                        'as': 'this_ksat',
                        'cond': {
                                    '$eq': [
                                        '$$this_ksat.ksat_id',
                                        ksat_match
                                    ]
                        }
                    }
                }
            }
        }
    ])

    for item in supp_mapping_ids:
        if len(item['KSATs']) == 0:
            print("length of supp ksats was 0 ")
            continue
        mapping = item
        url = mapping['url']
        prof = get_proficiency(item, mapping['ksat_id'])
        new_list.append('<a target="_blank" rel="noopener noreferrer" '
                        f'href="{url}"'
                        f' title="proficiency {prof}">{item["name"]}</a>')

    if len(new_list) > 0:
        find_id_and_insert(
            reqs,
            ksat['_id'],
            'training_links',
            new_list,
            upsert=True
        )


def update_trn_eval(ksat: dict, key: str, link_name_key: str):
    new_list = []

    # find all rel_link mapping to specific ksat
    rel_link_mapping_ids = rls.aggregate([
        {
            # this will find all rel_link documents of
            # _ids in training or eval ksat fields
            '$match': {
                '$or': [
                    {'KSATs.ksat_id': ksat['ksat_id'], 'map_for': key},
                    {'KSATs.object_id': ksat['_id'], 'map_for': key}
                ]
            }
        },
        {
            # we only want the KSATs mapping for this specific ksat
            # (remember the rellink document has all mapping for that rel_link)
            '$project': {
                'name': f'${link_name_key}',
                'KSATs': {
                    '$filter': {
                        'input': '$KSATs',
                        'as': 'this_ksat',
                        'cond': {
                            '$or': [
                                {
                                    '$eq': [
                                        '$$this_ksat.ksat_id',
                                        ksat['ksat_id']
                                    ]
                                },
                                {'$eq': ['$$this_ksat.object_id', ksat['_id']]}
                            ]
                        }
                    }
                }
            }
        }
    ])

    for item in rel_link_mapping_ids:
        if len(item['KSATs']) == 0:
            continue
        mapping = item['KSATs'][0]
        url = mapping['url']
        prof = mapping['item_proficiency'] if len(
            mapping['item_proficiency']) > 0 else 'N/A'
        new_list.append('<a target="_blank" rel="noopener noreferrer" '
                        f'href="{url}"'
                        f' title="proficiency {prof}">{item["name"]}</a>')

    find_id_and_modify(
        reqs,
        ksat['_id'],
        f'{key}_links',
        new_list,
        upsert=False
    )


def main():
    for ksat in reqs.find({}):
        update_trn_eval(ksat, 'training', 'subject')
    for ksat in reqs.find({}):
        update_trn_eval(ksat, 'eval', 'question_name')
    for ksat in reqs.find({}):
        add_supp_trn_eval(ksat, 'training', 'name')


if __name__ == "__main__":
    main()
