echo "Loading json files into mongo"
bash ./mttl/scripts/before_script.sh

echo "Generating KSAT links and children"
python3 ./mttl/scripts/pre_build/create_mttl_children.py
python3 ./mttl/scripts/pre_build/create_mttl_links.py

echo "Exporting mongo"
bash ./mttl/scripts/after_script.sh

echo "Generating datasets"
python3 ./mttl/scripts/build/create_mttl_dataset.py
python3 ./mttl/scripts/build/create_ttl_dataset.py
python3 ./mttl/scripts/build/create_extra_datasets.py
python3 ./mttl/scripts/build/create_metrics.py
python3 ./mttl/scripts/build/create_IDF_dataset.py
python3 ./mttl/scripts/build/roadmap_data.py

echo "Creating Autocomplete Data"
python3 ./mttl/scripts/pre_build/create_autocomplete.py

