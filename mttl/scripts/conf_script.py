#!/usr/bin/env python3
import os
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import csv

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504]
)
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")

# project_id = sys.argv[1]
# set project id to be linked to the mttl
project_id = 18738190

print("Getting issues for", project_id)
issues = []
page = 1
res = gl.get(
    gitlab_api+'/'+str(project_id)+'/issues',
    data={'state': 'closed'},
    # data={'state': 'opened'},
    headers={'PRIVATE-TOKEN': private_token}
)
while ('next' in res.links):
    page += 1
    issues.extend(res.json())
    res = gl.get(
        gitlab_api+'/'+str(project_id)+'/issues',
        data={
            # 'state': 'opened',
            'state': 'closed',
            'page': page
        },
        headers={'PRIVATE-TOKEN': private_token}
    )

issues.extend(res.json())
data_file = open('closed_issue_file.csv', 'w')
csv_writer = csv.writer(data_file)
count = 0

# will write the issue to the csv_file prior to running confidentiality change
for issue in issues:
    print("Editing issue #" + str(issue['iid']))
    if count == 0:
        header = issue.keys()
        csv_writer.writerow(header)
        count += 1
    print("Writing issue to csv")
    csv_writer.writerow(issue.values())
    res = gl.put(
        gitlab_api+'/'+str(project_id)+'/issues/'
        + str(issue['iid']),
        data={
            'confidential': True
        },
        headers={'PRIVATE-TOKEN': private_token}
    )

data_file.close()
