#!/usr/bin/python3

# SCRIPT DOESN'T WORK WITH DATA REDESIGN. TEMP DISABLED.
import os
import pymongo
import json

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles

log_data = {}
error = False

dir_name = "mttl/database/work-roles/"
workroles_dir = os.fsencode(dir_name)


def verify_ksat_exists(ksat_id: str):
    global error, reqs
    if reqs.count_documents({'ksat_id': ksat_id}) > 0:
        return True
    return False


def verify_mapping_exists(wr_mapping: dict):
    if not verify_ksat_exists(wr_mapping['ksat_id']):
        wrs.delete_one(wr_mapping)


def get_wr_files():
    directories = []
    for file in os.listdir(workroles_dir):
        filename = os.fsdecode(file)
        if filename.endswith('.json'):
            directories.append(filename.split('.')[0])
    return directories


def get_wr_mappings(file: str):
    wr_data = []
    for item in (
            wrs.find({"work-role": file}).sort("ksat_id", 1)
    ):
        wr_data.append({
            'work-role': item['work-role'],
            'ksat_id': item['ksat_id'],
            'proficiency': item['proficiency']
        })
    return wr_data


def main():
    global error, rls
    # loop all work_roles
    for wr in wrs.find({}):
        verify_mapping_exists(wr)

    work_role_files = get_wr_files()

    for file in work_role_files:
        data = get_wr_mappings(file)
        with open(dir_name + file + ".json", 'w+') as f:
            json.dump(data, f, indent=4)


if __name__ == "__main__":
    main()
