#!/usr/bin/python3

import os
import sys
import json
import pymongo
from bson.son import SON
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import convert_lists

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
wrs = db.work_roles

output_root = 'mttl/src/data'
log_output_path = os.path.join(output_root, 'create_IDF_dataset.log')

filename = os.path.splitext(os.path.basename(__file__))[0]


def key_picker(ksa: str):
    return {
        'T': 'Tasks',
        'A': 'Abilities',
        'S': 'Skills',
        'K': 'Knowledge'
    }[ksa[0]]


def query_and_iterate_wrspec(wrspec: str, outdata: dict):
    outdata[wrspec] = {
        'wr_spec': wrspec,
        'Tasks': '',
        'Abilities': '',
        'Skills': '',
        'Knowledge': ''
    }

    for ksat in reqs.aggregate([
        {'$lookup': {
            'from': 'work_roles',
            'localField': 'ksat_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles.work-role': wrspec}}
    ]):
        ksat_id = ksat['ksat_id']
        ksat_key = key_picker(ksat_id)
        if len(outdata[wrspec][ksat_key]) == 0:
            outdata[wrspec][ksat_key] = f'{ksat_id}'
        else:
            outdata[wrspec][ksat_key] += f', {ksat_id}'


def create_idf_ksats():
    idf_ksats = list(reqs.aggregate([
        {
            '$lookup': {
                'from': 'rel_links',
                'localField': 'ksat_id',
                'foreignField': 'KSATs.ksat_id',
                'as': 'rl'
            }
        }, {
            '$lookup': {
                'from': 'work_roles',
                'localField': 'ksat_id',
                'foreignField': 'ksat_id',
                'as': 'work-roles'
            }
        }, {
            '$match': {'rl.course': 'IDF'}
        }, {
            '$project': {'rl': 0, 'training': 0, 'eval': 0}
        },
        {'$sort': SON([('ksat_id', 1)])}
    ]))

    for ksat in idf_ksats:
        for tmpwr in ksat['work-roles']:
            if tmpwr['work-role'] == 'CCD' and 'proficiency' in tmpwr:
                ksat['proficiency'] = tmpwr['proficiency']
        convert_lists(ksat)
        ksat['_id'] = str(ksat['_id'])
        if len(ksat['parent']) > 0:
            ksat['parent'] = reqs.find(
                {'_id': {'$in': ksat['parent']}}
            ).distinct('ksat_id')
        ksat['work-roles/specializations'] = ksat['work-roles']

        if 'children' not in ksat:
            ksat['children'] = ''

        if 'training_links' not in ksat:
            ksat['training_links'] = ''

        if 'eval_links' not in ksat:
            ksat['eval_links'] = ''

    return(idf_ksats)


def main():
    global error, output_root, log_output_path
    output_path = os.path.join(
        output_root, 'IDF_dataset.min.json'
    )
    idf_ksats = create_idf_ksats()
    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w+') as fd:
        json.dump(idf_ksats, fd, sort_keys=False, separators=(',', ':'))


if __name__ == "__main__":
    main()
