#!/usr/bin/python3

import os
import sys
import json
import pymongo
from bson.son import SON
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import convert_lists

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
wrs = db.work_roles

ttldata = {}


def query_and_change_ksat(collection: object, wrspec: str):
    global ttldata
    # find and iterate through wrspec ksats
    ttldata[wrspec] = list(collection.aggregate([
        {'$lookup': {
            'from': 'work_roles',
            'localField': 'ksat_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles.work-role': wrspec}},
        {'$unset': 'work-roles._id'},
        {'$project': {'training': 0, 'eval': 0}},
        {'$sort': SON([('ksat_id', 1)])}
    ]))
    for ksat in ttldata[wrspec]:
        for tmpwr in ksat['work-roles']:
            if tmpwr['work-role'] == wrspec and 'proficiency' in tmpwr:
                ksat['proficiency'] = tmpwr['proficiency']
        convert_lists(ksat)
        ksat['_id'] = str(ksat['_id'])
        if len(ksat['parent']) > 0:
            ksat['parent'] = reqs.find(
                {'_id': {'$in': ksat['parent']}}
            ).distinct('ksat_id')
        ksat['work-roles/specializations'] = ksat['work-roles']
        if 'children' not in ksat:
            ksat['children'] = ''
        if 'training_links' not in ksat:
            ksat['training_links'] = ''
        if 'eval_links' not in ksat:
            ksat['eval_links'] = ''


def main():
    global ttldata
    output_root = 'mttl/src/data'
    output_path = os.path.join(output_root, 'TTLs.min.json')

    # iterate through all work-roles
    for key in list(wrs.distinct('work-role')):
        # query for work-roles ksats and iterate
        query_and_change_ksat(reqs, key)

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as ttfile:
        json.dump(ttldata, ttfile, sort_keys=False, separators=(',', ':'))


if __name__ == "__main__":
    main()
