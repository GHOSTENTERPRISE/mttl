#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import convert_lists

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    wrs = db.work_roles

    output_root = 'mttl/src/data'
    output_path = os.path.join(output_root, 'MTTL.min.json')
    mttl = reqs.find({})
    mttl_data = []

    # find all ksats and iterate
    for ksat in mttl:
        convert_lists(ksat)
        ksat['_id'] = str(ksat['_id'])
        if len(ksat['parent']) > 0:
            ksat['parent'] = reqs.find(
                {'_id': {'$in': ksat['parent']}}
            ).distinct('ksat_id')
        if 'children' not in ksat:
            ksat['children'] = ''
        if 'training_links' not in ksat:
            ksat['training_links'] = ''
        if 'eval_links' not in ksat:
            ksat['eval_links'] = ''

        ksat['work-roles'] = wrs.find(
            {"ksat_id": ksat['ksat_id']}
        ).distinct('work-role')

        ksat['proficiency_data'] = []
        for wr in ksat['work-roles']:
            prof_data = {}
            prof_data['workrole'] = wr
            prof_data['proficiency'] = wrs.find_one(
                {'work-role': wr, 'ksat_id': ksat['ksat_id']}
            )['proficiency']
            ksat['proficiency_data'].append(prof_data)

        mttl_data.append(ksat)

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as mttfile:
        json.dump(mttl_data, mttfile, sort_keys=False, separators=(',', ':'))


if __name__ == "__main__":
    main()
