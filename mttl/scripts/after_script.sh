#!/bin/bash

echo "test before initial if..."

if [ -z ${MONGO_HOST} ]; then
    MONGO_HOST=localhost;
fi

cd mttl/database
# export KSATs into respective ksat files from requirements collection
if [ -d "requirements" ]; then
    for f in requirements/*.json
    do
        # mongoexport --host $MONGO_HOST --db mttl --collection requirements --out $f --sort "{'ksat_id': 1}" --query "{'ksat_id':{'\$regex':'[{$f:13:1}][0-9]{4}'}}" --jsonArray --pretty
        mongoexport --host $MONGO_HOST --db mttl --collection requirements --out $f --sort "{'ksat_id': 1}" --query "{'ksat_id':{'\$regex':'[${f:13:1}][0-9]{4}'}}" --jsonArray --pretty

    done
fi

# export rel-links into respective ksat files from rel_links collection
if [ -d "rel-links/training" ]; then
    for f in rel-links/training/*.rel-links.json
    do
        course=$(echo $f | cut -d'/' -f 3 | cut -d'.' -f 1)
        mongoexport --host $MONGO_HOST --db mttl --collection rel_links --out $f --sort "{'_id': 1}" --query "{'course':'$course'}" --jsonArray --pretty
    done
fi
if [ -d "rel-links/eval" ]; then
    for f in rel-links/eval/*.rel-links.json
    do
        test_id=$(echo $f | cut -d'/' -f 3 | cut -d'.' -f 1)
        mongoexport --host $MONGO_HOST --db mttl --collection rel_links --out $f --sort "{'_id': 1}" --query "{'test_id':'$test_id'}" --jsonArray --pretty
    done
fi

cd -
