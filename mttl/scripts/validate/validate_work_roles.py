#!/usr/bin/python3

import os
import sys
import json
import jsonschema
import argparse
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import helpers
from json_templates import ksat_wr_spec_template


def validate_insert_data(data: dict, schema_func):
    '''
    validate all insertion data
    '''
    wr_list = helpers.get_work_roles()

    try:
        jsonschema.validate(instance=data, schema=schema_func(wr_list))
    except jsonschema.exceptions.ValidationError as err:
        print(err)


def main():
    parser = argparse.ArgumentParser(
        description='Validate rel-link JSON files')
    parser.add_argument(
        'path',
        metavar='PATH',
        type=str,
        help='path to the rel-link directory')
    parsed_args = parser.parse_args()

    for dir_name, subdir_list, file_list in os.walk(parsed_args.path):
        for file_name in file_list:
            with open(os.path.join(dir_name, file_name)) as fd:
                data = json.load(fd)
            validate_insert_data(data, ksat_wr_spec_template)


if __name__ == "__main__":
    main()
