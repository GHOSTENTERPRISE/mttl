#!/usr/bin/python3

import os
import json
import pymongo

import re
import string
from spellchecker import SpellChecker

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles


filename = os.path.splitext(os.path.basename(__file__))[0]
log_data = {}
error = False


spell = SpellChecker()
spell.word_frequency.load_dictionary(
    './mttl/scripts/validate/90cos_dictionary.gz')
spell.word_frequency.load_text_file('./mttl/scripts/validate/90coswords.txt')
spell.word_frequency.load_words(['Big-O', 'subnetting',
                                 'multi-dimensional',
                                 'pre-processor', 'burn-down',
                                 'Back-Shop', 'Real-Time',
                                 'Front-Door', 'non-standard',
                                 'pre-defined',
                                 'multi-threaded', 'No-Go', 'burn-down',
                                 'sub-task', 'non-standard'])
spell.word_frequency.load_words(['--name', '--rm', '--user',
                                 '--volume',
                                 '-bit', '-e', '-p', 'C-types',
                                 'CMF-Roles', 'COS-Supported', 'Get-Cmdlet',
                                 'Get-Command', 'Get-Help',
                                 'Get-Member', 'Multi-threading',
                                 'Non-secure', 'Object-oriented',
                                 'Platform-Access-Payloads',
                                 'Pre-Installation', 'Public-Key',
                                 'Return-Oriented',
                                 'String-based', 'XMM-XMM',
                                 'Zero-out', 'all-at-once',
                                 'application-specific', 'built-in',
                                 'client-server', 'context-free',
                                 'cross-compiler', 'cross-site',
                                 'de-serialization',
                                 'de-serialize', 'decision-making',
                                 'defense-in-depth', 'gitlab-ciyml',
                                 'high-level', 'high-quality',
                                 'just-in-time', 'low-level',
                                 'multi-byte',
                                 'multi-threading', 'nation-state',
                                 'non-encrypted', 'non-repudiation',
                                 'non-technical', 'object-oriented',
                                 'off-by-one', 'platform-specific',
                                 'position-independent',
                                 'real-world', 'return-oriented',
                                 'service-oriented', 'side-effects',
                                 'state-machine', 'subnetting',
                                 'sub-registers', 'thread-safe',
                                 'use-after-free',
                                 'user-mode', 'x-', 'zero-out'])

remove = string.punctuation
remove = remove.replace("-", "")


def trim_extra_characters(words: list):
    words = re.sub(r'/', ' ', words)
    words = re.sub(r'[0-9]', '', words)
    words = re.sub(r'\(U\)', '', words)
    words = re.sub(r'\s+', ' ', words)
    words = words.strip().split()
    words = [''.join(c for c in s if c not in remove) for s in words]
    words = list(filter(None, words))
    map(str.strip, words)
    return words


def check_spelling_ksats(req: dict):
    ksat_id = req['ksat_id']
    words = req['description']
    words = trim_extra_characters(words)

    for word in words:
        if word not in spell:
            cor = spell.correction(word)
            if cor not in word and word[0] != '-' and word[-1] != '-':
                if ksat_id not in log_data:
                    log_data[ksat_id] = {}
                    log_data[ksat_id].update({
                        word:
                        "An alternative spelling for \
                        '{}' is '{}'".format(
                            word, cor)
                    })
                elif word not in log_data:
                    log_data[ksat_id].update({
                        word:
                        "An alternative spelling for \
                        '{}' is '{}'".format(
                            word, cor)
                    })


def main():
    global error, rls
    for req in reqs.find({}):
        check_spelling_ksats(req)
    # spell.export('90cos_dictionary.gz', gzipped=True)
    # ^ Create custom dictionary ^
    if len(log_data) > 0:
        with open(f'{filename}.json', 'w') as log_file:
            json.dump(log_data, log_file, sort_keys=False, indent=4)
        exit(1)


if __name__ == "__main__":
    main()
