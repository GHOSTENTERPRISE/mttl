#! /usr/bin/python3

import os
import json
import sys
import get_old_record


# old KSATs
old_tasks_file = "oldTasks.json"
old_abilities_file = "oldAbilities.json"
old_skills_file = "oldSkills.json"
old_knowledge_file = "oldKnowledge.json"
# old work roles
old_90cos_work_role_file = "old90COS.json"
old_ac_work_role_file = "oldAC.json"
old_ccd_work_role_file = "oldCCD.json"
old_cst_work_role_file = "oldCST.json"
old_instructor_work_role_file = "oldInstructor.json"
old_jsre_work_role_file = "oldJSRE.json"
old_mccd_work_role_file = "oldMCCD.json"
old_po_work_role_file = "oldPO.json"
old_sad_work_role_file = "oldSAD.json"
old_sccdl_work_role_file = "oldSCCD-L.json"
old_sccdw_work_role_file = "oldSCCD-W.json"
old_see_work_role_file = "oldSEE.json"
old_sm_work_role_file = "oldSM.json"
old_spo_work_role_file = "oldSPO.json"
old_ssm_work_role_file = "oldSSM.json"
old_ssre_work_role_file = "oldSSRE.json"
old_sysadmin_work_role_file = "oldSysadmin.json"
old_tae_work_role_file = "oldTAE.json"


def delete_old_files():
    old_olds = [
        old_ccd_work_role_file, old_tasks_file,
        old_abilities_file, old_skills_file, old_knowledge_file,
        old_90cos_work_role_file, old_ac_work_role_file,
        old_ccd_work_role_file, old_cst_work_role_file,
        old_instructor_work_role_file, old_jsre_work_role_file,
        old_mccd_work_role_file, old_po_work_role_file, old_sad_work_role_file,
        old_sccdl_work_role_file, old_sccdw_work_role_file,
        old_see_work_role_file, old_sm_work_role_file,
        old_spo_work_role_file, old_ssm_work_role_file,
        old_ssre_work_role_file, old_sysadmin_work_role_file,
        old_tae_work_role_file]
    for old in old_olds:
        if os.path.exists(old):
            os.remove(old)


def get_work_role_file(workrole):
    if workrole == "90COS":
        return old_90cos_work_role_file
    elif workrole == "AC":
        return old_ac_work_role_file
    elif workrole == "CCD":
        return old_ccd_work_role_file
    elif workrole == "CST":
        return old_cst_work_role_file
    elif workrole == "Instructor":
        return old_instructor_work_role_file
    elif workrole == "JSRE":
        return old_jsre_work_role_file
    elif workrole == "MCCD":
        return old_mccd_work_role_file
    elif workrole == "PO":
        return old_po_work_role_file
    elif workrole == "SAD":
        return old_sad_work_role_file
    elif workrole == "SCCDL":
        return old_sccdl_work_role_file
    elif workrole == "SCCDW":
        return old_sccdw_work_role_file
    elif workrole == "SEE":
        return old_see_work_role_file
    elif workrole == "SM":
        return old_sm_work_role_file
    elif workrole == "SPO":
        return old_spo_work_role_file
    elif workrole == "SPO":
        return old_spo_work_role_file
    elif workrole == "SPO":
        return old_spo_work_role_file
    elif workrole == "SPO":
        return old_spo_work_role_file
    elif workrole == "SPO":
        return old_spo_work_role_file
    elif workrole == "SPO":
        return old_spo_work_role_file
    else:
        print("Unable to identify {} as a valid old work role. ", workrole)
        sys.exit()


def find_work_role_ksats(workrole):
    work_role_ksats = []
    work_role_file = get_work_role_file(workrole)
    with open(work_role_file) as wr_check:
        wr_data = json.load(wr_check)
        ttl = (wr_data['TTL'])
        for ksat in ttl:
            current_ksat = (ksat["ksat_id"])
            work_role_ksats.append(current_ksat)
        return list(set(work_role_ksats))


def load_knowledge():
    ksat_file = old_knowledge_file
    with open(ksat_file) as ksat_check:
        knowledge_data = json.load(ksat_check)
        return knowledge_data


def load_skills():
    ksat_file = old_skills_file
    with open(ksat_file) as ksat_check:
        skills_data = json.load(ksat_check)
        return skills_data


def load_abilities():
    ksat_file = old_abilities_file
    with open(ksat_file) as ksat_check:
        abilities_data = json.load(ksat_check)
        return abilities_data


def load_tasks():
    ksat_file = old_tasks_file
    with open(ksat_file) as ksat_check:
        tasks_data = json.load(ksat_check)
        return tasks_data


def get_work_role_expectations(ksats):
    great_expectations = []
    knowledge_data = load_knowledge()
    skills_data = load_skills()
    abilities_data = load_abilities()
    task_data = load_tasks()
    # Q: why not a PG query ?
    # A: that's fine for old/current - \
    # but there is not infinite database  for all KSAT versions
    # Either way the old data has to be loaded
    for line in knowledge_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    for line in skills_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    for line in abilities_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    for line in task_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    return great_expectations


def get_old_work_role_ksats(workrole):
    if workrole not in ["90COS", "AC", "CCD", "CST",
                                 "INSTRUCTOR", "JSRE",
                                 "MCCD", "PO", "SAD", "SCCDL",
                                 "SCCDW", "SEE",
                                 "SM", "SPO", "SSM", "SSRE",
                                 "SYSADMIN", "TAE"]:
        print("Error Invalid Work Role specified ",
              workrole)
        sys.exit()
    else:
        print("Getting KSATs for ", workrole)
        work_role_ksats = find_work_role_ksats(workrole)
        work_role_expectations = get_work_role_expectations(work_role_ksats)
        return work_role_expectations


def main(file_to_get, time_ago):
    delete_old_files()
    file_to_get = file_to_get.upper()
    file_to_get = ''.join(e for e in file_to_get if e.isalnum())
    print("Getting current {} work role file.".format(file_to_get))
    get_old_record.main(file_to_get, time_ago)
    for skill in ["K", "S", "A", "T"]:
        get_old_record.main(skill, time_ago)
    return(get_old_work_role_ksats(file_to_get))


if __name__ == "__main__":
    file_to_get = sys.argv[1]
    time_ago = sys.argv[2]
    main(file_to_get, time_ago)
