#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import helpers

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links

filename = os.path.splitext(os.path.basename(__file__))[0]
log_data = {}
error = False


def verify_ksat_exists(ksat_id: str):
    global error, reqs
    if reqs.count_documents({'_id': ksat_id}) > 0:
        return True
    return False


def verify_mapping_exists(rel_link: dict):
    for mapping in rel_link['KSATs']:
        if (
            'object_id' not in mapping
           and 'ksat_id' in mapping
           ):
            mapping['object_id'] = helpers.oid_to_id(mapping['ksat_id'])
        if not verify_ksat_exists(mapping['object_id']):
            mapping_type = str(rel_link['map_for'])
            if (mapping_type == 'training'):
                try:
                    identifier = mapping_type + ": " + str(rel_link['course'])
                except KeyError:
                    identifier = mapping_type + ": " + "Unknown"
            elif (mapping_type == 'eval'):
                try:
                    identifier = mapping_type + ": " + str(rel_link['test_id'])
                except KeyError:
                    identifier = mapping_type + ": " + "Unknown"
            if identifier not in log_data:
                log_data[identifier] = {}
            log_data[identifier].update({
                str(mapping['ksat_id']): 'does not exist in requirements'
            })


def main():
    global error, rls
    # loop all rel_links
    for rel_link in rls.find({}):
        verify_mapping_exists(rel_link)

    if len(log_data) > 0:
        with open(f'{filename}.json', 'w') as log_file:
            json.dump(log_data, log_file, sort_keys=False, indent=4)
        exit(1)


if __name__ == "__main__":
    main()
