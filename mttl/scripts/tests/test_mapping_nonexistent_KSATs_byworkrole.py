#!/usr/bin/python3

import os
import json
import pymongo

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles


filename = os.path.splitext(os.path.basename(__file__))[0]
log_data = {}
error = False


def verify_ksat_exists(ksat_id: str):
    global error, reqs
    if reqs.count_documents({'ksat_id': ksat_id}) > 0:
        return True
    return False


def verify_mapping_exists(wr: dict):
    if not verify_ksat_exists(wr['ksat_id']):
        ksat_id = wr['ksat_id']
        workrole = wr['work-role']
        if workrole not in log_data:
            log_data[workrole] = {}
            log_data[workrole].update({
                str(ksat_id): 'does not exist in requirements'
            })
        elif ksat_id not in log_data:
            log_data[workrole].update({
                str(ksat_id): 'does not exist in requirements'
            })


def main():
    global error, rls
    # loop all work_roles
    for wr in wrs.find({}):
        verify_mapping_exists(wr)

    if len(log_data) > 0:
        with open(f'{filename}.json', 'w') as log_file:
            json.dump(log_data, log_file, sort_keys=False, indent=4)
        exit(1)


if __name__ == "__main__":
    main()
