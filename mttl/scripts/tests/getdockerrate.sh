#! /bin/bash 
hash sudo 2>/dev/null && (hash jq 2>/dev/null || sudo apt -y install jq) || (hash jq 2>/dev/null || apt -y install jq) 
TOKEN=$(curl "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq --raw-output .token) && curl --head --header "Authorization: Bearer $TOKEN" "https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest" 2>&1 | grep RateLimit

exit 0 