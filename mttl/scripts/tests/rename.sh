#!/bin/bash
mv $1.py $2.py
sed -i "s/$1/$2/g" ../../../server.js
sed -i "s/$1/$2/g" ../../../.gitlab-ci.yml
find -exec sed -i "s/$1/$2/g" {} \;