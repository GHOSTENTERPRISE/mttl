#!/usr/bin/env python3
import os
import requests
import sys
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from urllib.parse import unquote


def main(changes):
    print(len(sys.argv))
    changes = unquote(changes)
    gl = requests.Session()
    retries = Retry(
        total=5,
        backoff_factor=1,
        status_forcelist=[401, 502, 503, 504]
    )
    gl.mount('https://', HTTPAdapter(max_retries=retries))
    gitlab_api = 'https://gitlab.com/api/v4/projects'
    if os.getenv("ROBOTOKEN"):
        private_token = os.getenv("ROBOTOKEN")
        print("TOKEN exists ", len(os.getenv("ROBOTOKEN")))
    elif os.getenv("TOKEN"):
        private_token = os.getenv("TOKEN")
        print("TOKEN exists ", len(os.getenv("TOKEN")))
    elif os.getenv("MTTL_BOT_TOKEN"):
        print("MTTL BOT TOKEN exists ", len(os.getenv("MTTL_BOT_TOKEN")))
        private_token = os.getenv("MTTL_BOT_TOKEN")
    elif os.getenv("K8S_SECRET_ROBOTOKEN"):
        print("using K8S_SECRET_ROBOTOKEN ", len(
            os.getenv("K8S_SECRET_ROBOTOKEN")))
        private_token = os.getenv("K8S_SECRET_ROBOTOKEN")
    else:
        print("Unable to summon MTTL robot i.e. \
no worthwhile token found. Exiting.")
        sys.exit()
    # changes = sys.argv[1]
    project_id = 18738190
    data = {
        'title': 'Change Detected in CCD KSATs ',
        'description': "An addition, deletion, KSAT change in description greater than 5 characters,\
         or a change in proficiency has been detected \
         in CCD related KSATs.  " +
        "\n\n Attention:  \
@nwmiller @TrestanPatton @jguerra7 \
@kriegliedthosshys @christiniat @fullentropy \
@neal.scheider.95 @Ivan_Wright @wallisii\
" + """

Description: """ + changes + """
    """ +
        """
/label ~customer ~"office::CYT" ~"codeowners::CCD"  ~"CYT::MTTL"
/milestone %"CYT Backlog"
    """
    }
    print("Posting submission to GitLab: ", data)
    res = gl.post(
        gitlab_api+'/'+str(project_id)+'/issues',
        data=data,
        headers={'PRIVATE-TOKEN': private_token}
    )
    print(res.json())


if __name__ == "__main__":
    changes = sys.argv[1]
    main(changes)


# @nwmiller @TrestanPatton @jguerra7 \
# @kriegliedthosshys @christiniat @fullentropy \
# @neal.scheider.95 @Ivan_Wright
