# combined-template

- For further reference on MDBook, see [here](https://gitlab.com/90cos/cyt/training-systems/templates/mdbook-template)
- For further reference on Reveal JS Slides, see [here](https://gitlab.com/90cos/cyt/training-systems/templates/slides-template)

# Add multiple RevealJS SlideDecks

You are able to create multiple slide decks in the same MDBook. These slide decks are logically separated and do not interact with each other. They can be access by adding a ```/slides``` at the end of your URL (depending on where you place them). There have been multiple slide deck added to this template to help you understand how to implement it yourself. 

You will notice that there are separate slides located at different levels of the ```mdbook/src``` folder.  
They are listed below:  

```sh
mdbook/src/cli/slides 
mdbook/src/for_developers/slides
mdbook/src/format/slides  
mdbook/src/slides  
```
Each one of these ```slides``` folders will have an ```index.html``` file defining the slide's content. 
To understand how to change this file to reflect your slide, check out the 'Reveal JS Slides, references' above.

Place a copy of this ```slides``` folder anywhere within the ```mdbook/src``` folder and the GitLab pipeline will create slides which can be accessed at the URL representative of the relative path. 

