# Definitions
## Requirement Source
A document, publication, and/or other "source" of a compliance requirement that the MTTL seeks to satisfy.  A requirement source is the object to reference for detailed KSAT requirement information normally authored by a requirements owner.  All Sources must be linked to a Requirement Owner.  It is allowable for a requirements Owner to mandate a requirements source which is not their own document.
## Requirement Owner
An office or function which has stated that a requirements source is applicable to our personnel or unit.  For example, USCYBERCOMMAND J7 is the requirements owner for the JCT&CS which is a requirements source (also authored by them) for presentation of forces to USCYBERCOMMAND (a 90th mission).
Business/Design Logic

***If a KSAT has a Requirement Source it must have a Requirement Owner.***  
If a KSAT has a Requirement Owner, they must coordinate/approve (via any means such as phone call, email, etc.) any changes to the KSAT as they are the levying authority.  This may be accomplished out of Gitlab but merge approvers are responsible and accountable for changes they allow to be merged to the MTTL.  *A KSAT ***may*** have a requirements owner without a requirements source (indicating the KSAT should stand alone without further clarification or explanation).*
If a KSAT has neither a Requirement Source nor a requirements owner it is considered a **Derived Requirement** and no additional coordination is required prior to changes (Work role owner and other peer review requirements still apply).

## Why not every KSAT?
Requirements sources and Owners are an additional administrative burden to account for but were intended to assist with measuring compliance against requirements.  KSATs without sources and owners require less coordination and therefore can be modified with less friction.  Friction is appropriate for those requirements that have requirements sources and owners as these external elements must be considered.
Some of this language can be found in closed issues such as 90cos/mttl#357 & 90cos/mttl#134 (closed). 