# KSAT Parent-Child Relationships in the MTTL

Many, but not KSATs will be related to one another in a parent/child relationship. Use the following guidelines when creating these relationships.



| Item | Allowable Parent(s) |
| ------ | ------ |
| Knowledge | Skill, Ability, Task |
| Skill | Ability, Task | 
| Ability | Task | 
| Task | None | 

An example of this relationship could be a **Knowledge** KSAT to understand how to use and implement logical operators. A parent of this Knowledge KSAT may be a **Skill** to write a boolean expression using a logical operator. A parent of this Skill KSAT may be an **Ability** KSAT to write a program that must handle multiple conditions in data processing. Finally, the parent of the Ability KSAT may be a JCT&CS defined **Task** to analyze, modify, develop, debug and document software and applications in C programming language.  

_**Note: that KSATS that can have children can have multiple children, and KSATs that can have parents can have multiple parents.**_ 
