## Angular Documentation

This section describes the function of each component in the MTTL Project. Below - you will find information on every imported and generated component that we use in this project. All of the general information can be found in the imports of the `app.module.ts` file, part of which is included below

```javascript
/* ANGULAR IMPORTS */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

/* ANGULAR MATERIAL */
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button'; 
import {MatInputModule} from '@angular/material/input'; 
import {MatCardModule} from '@angular/material/card';

/* ANGULAR FORMS */
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';

/* Routing */
import { AppRoutingModule } from './app-routing.module';

/* THIRD PARTY */
import { NgxPaginationModule } from 'ngx-pagination';
import { CookieService } from 'ngx-cookie-service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CarouselModule } from 'ngx-bootstrap/carousel';

/* SERVICES */
import { ApiService } from "./api.service";

/* COMPONENTS */
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MttlComponent } from './mttl/mttl.component';
import { KsatLinkPromptComponent } from './ksat-link-prompt/ksat-link-prompt.component';
import { NewKsatPopupComponent } from './new-ksat-popup/new-ksat-popup.component';
import { MetricsComponent } from './metrics/metrics.component';
import { AllMetricsComponent } from './all-metrics/all-metrics.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

/* PIPES */
import { FilterByArrayPipe } from './pipes/filter-by-array.pipe';
import { FilterMTTLPipe } from './pipes/filter-mttl.pipe';
import { CoverageCalculatorPipe } from './pipes/coverage-calculator.pipe';
import { OrderBy } from './pipes/order-by.pipe';

/*Export to CSV*/
import { ExportToCSVservice } from './export-to-csv.service';
import { ExportToXLSXservice } from './export-to-xlsx.service';

```