## Pipes

We use [Angular Pipes](https://angular.io/guide/pipes) to filter ksat data in this project. Here are the pipes that we use and what they do:

| Pipe Name              | Input                                                                              | Process                                                                                     | Output                                        |
|------------------------|------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|-----------------------------------------------|
| FilterByArrayPipe      | Items: List of KSATs <br>  Field: Dict Key to filter on <br>  Value: value to find | Checks the dict key *field*, which should contain an array, for value                       | A list of KSATs that contain value in field   |
| FilterMTTLPipe         | Items: List of KSATs <br>  Filter_obj: A list of key: value pairs to filter on     | Matches every ksat that contains each key: value pair in the filter_obj                     | A list of KSATs that match                    |
| CoverageCalculatorPipe | Items: List of KSATs <br>  Field: Coverage type                                    | Finds a list of ksats that match the coverage type (defined below) based on rel-link counts | A list of KSATs that match that coverage type |
| OrderByPipe            | List of ksats, + or -                                                              | Returns a sorted list of dicts based of field                                               | Sorted list                                   |
| FAQSearch | Array of strings, substring | Checks each item in array of faq items for substring | All faq items that contain the substring |
| AutocompletePipe | Array of strings, substring | Checks each item in array for substring | All items in the array that contain the substring |
