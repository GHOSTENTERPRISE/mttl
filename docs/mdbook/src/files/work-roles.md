## Work roles .json

```json
 {
        "work-role": "CYT",
        "ksat_id": "K0909",
        "proficiency": "3"
    }
```

The publicly available sources for work role requirements are AIR COMBAT COMMAND INSTRUCTION [Volume 1](https://static.e-publishing.af.mil/production/1/acc/publication/acci17-202v1/acci17-202v1.pdf), [Volume 2](https://static.e-publishing.af.mil/production/1/af_a2_6/publication/afi17-2cdav2/afi17-2cdav2.pdf), and [Volume 3](https://static.e-publishing.af.mil/production/1/af_a2_6/publication/afi17-2afincv3/afi17-2afincv3.pdf). 


For more information about ***work roles and specializations***, see [here](https://90cos-mttl.90cos.com/documentation/mttl/index.html)
For more information about ***new work roles*** see [here](../wrspec/new-wrspec.md).  
For more information about ***work role upgrades***, see [here](../wrspec/wrspec-upgrade.md). 

Currently defined work roles and specializations include:  
```
├── 90COS.json                 - Applicable to everyone in the 90th 
├── AC.json                    - Agile Coach
├── CCD.json                   - Cyber Capability Developer
├── CST.json                   - Client Systems Technician
├── Instructor.json            - Instructor
├── JSRE.json                  - Junior Site Reliability Engineer
├── MCCD.json                  - Master Cyber Capabilities Developer
├── PO.json                    - Product Owner 
├── SCCD-L.json                - Linux Senior Cyber Capabilities Developer
├── SCCD-W.json                - Windows Senior Cyber Capabilities Developer
├── SEE.json                   - Stan/Eval Evaluator
├── SM.json                    - Scrum Master  
├── SAD.json                   - Specialty Area Developer
├── SPO.json                   - Senior Product Owner 
├── SSM.json                   - Senior Scrum Master
├── SSRE.json                  - Senior Site Reliability Engineer
├── Sysadmin.json              - System Administrator
└── TAE.json                   - Testing Automation Engineer

```

### Data Structure

```json
{
    "name": "90COS",
    "fullName": "90th Cyberspace Operations Squadron",
    "description":"Baseline knowledge for all people in the squadron",
    "roadmap_nodes": [
        {
            "id": "CCD",
            "group": "Developer",
            "upgrades": ["SCCD-L", "SCCD-W", "TAE", "SAD"]
        }
    ],
    "paths": [
        {
            "name": "Commercial Path",
            "path": [
                {
                    "name": "Request CSPO Training",
                    "description": "Request Certified Scrum Product Owner and Certification",
                    "link": "https://jira.90cos.cdl.af.mil/servicedesk/customer/portal/11"
                },
                { ... }
            ]
        },
        { ... }
    ],
    "TTL": [
        {
            "work-role": "CCD",
            "ksat_id": "A0359",
            "proficiency": "2",
            "overarching_milestone": "Project Management"
        } // Cut off rest of the TTL. Other entries follow pattern above
    ]
}
```

`name`: The short name of the work role, generally an acronym.
`fullName`: The full name of the work role
`description`: A short description of the work role
`roadmap_nodes`: Data structure that defines [roadmap overview](../roadmap/overview.md) nodes
`paths`: Data structure that defines [workrole roadmap](../roadmap/roadmap.md) paths
`overarching-milestone`: Contains the [Overarching Milestone](../mttl/OAM.md) for the ksat in the work role.
`TTL`: A list of Knowledge, Skills, Abilities and Tasks that members of this work role require in order to effectively perform their duties.

### To add an entry:

Simply append a dict to the TTL entry with the following data by manually editing the JSON file.
Pay attention to brackets. Bracket matching support is [highly recommended](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2).  

For, example, if we wanted to add a KSAT to the above example:

```json
{
    "name": "90COS",
    "fullName": "90th Cyberspace Operations Squadron",
    "description":"Baseline knowledge for all people in the squadron",
    "roadmap_nodes": [
        {
            "id": "CCD",
            "group": "Developer",
            "upgrades": ["SCCD-L", "SCCD-W", "TAE", "SAD"]
        }
    ],
    "paths": [
        {
            "name": "Commercial Path",
            "path": [
                {
                    "name": "Request CSPO Training",
                    "description": "Request Certified Scrum Product Owner and Certification",
                    "link": "https://jira.90cos.cdl.af.mil/servicedesk/customer/portal/11"
                },
                { ... }
            ]
        },
        { ... }
    ],
    "TTL": [
        {
            "work-role": "CCD",
            "ksat_id": "A0359",
            "proficiency": "2",
            "overarching_milestone": "Project Management"
        },
        {
            "work-role": "CCD",
            "ksat_id": "A0001",
            "proficiency": "2",
            "overarching_milestone": "Project Management"
        }
    ]
}
```

