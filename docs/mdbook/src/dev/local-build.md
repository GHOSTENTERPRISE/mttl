# How to run the pipeline in your dev environment
If your development environment is working appropriately in a Linux environment, you can run the following.
```sh
./build_frontend.sh
```
This script will run all necessary scripts to build the MTTL frontend in the same way the Gitlab CI/CD pipeline is configured. *Please note* that this MDbook is generated during the pipeline and will not be visible using this method. You may, however, see them via review apps.

### Rerunning the Build
Once the data have been generated, the build time can be reduced by using the 
```sh
./rebuild_frontend.sh
```
command.  
Alternatively, if ExpressJS functionality is not required, running `ng s -o` in the `mttl` directory will create an Angular app. This is useful for evaluating changes to the frontend only.
  

### Troubleshooting
If there are errors running the the pipeline script there are a few things to test to make sure the development environment is running as expected

### Missing Data?

Missing data can be generated via the `build_frontend.sh` script, however you can also [download these files as artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#downloading-artifacts) from a recent CI job.

The MTTL needs to have three files in the mttl/src/data/ folder in order to run properly. It needs 

- `MTTL.min.json`, which is generated in the `Create MTTL Dataset` job in the  `generate-data` stage
- `roadmap.min.json`, which is generated in the `Roadmap Data` job in the `generate-data` stage
- `autocomplete.min.json`, which is generated in the `Create Autocomplete` job in the `prebuild` stage

#### MongoDB connection issues
If you are getting mongodb connection errors it may be possible that the mongodb service is not running. If you are running mongodb on your host, make sure it is running with the default port and host. If you are running mongodb in a container with docker do the following
1. run ```docker ps``` in a host terminal. You should see a mongo container running. If not, you can start a mongo container easily in the next step.
2. start a mongo container with ```docker run -d --network host mongo```. This will start a mongo container in detached mode (running in the background). 
3. try the ```run_pipeline_scripts.sh``` script again  

#### Directly connecting to MongoDB  
To directly connect to MongoDB in the running Docker environment:  
- `mongo mongodb://mongo:27017/mttl`  
If you connect, you will see "Welcome to the MongoDB shell." and other text including warnings.  
To check that everything is running properly, type:  
`show dbs`  
The last and only database with data should be ***mttl***  
To switch to MTTL, type:  
`use mttl`  
To see the available collections:  
`show collections`  
To see all the data in a collection:  
db.<collection name>.find()  
To see all the keys in a collection:  
`doc=db.<collection name>.findOne(); for (key in doc) print(key);`  

  
For more information about Mongo commands see [here](../dev/queries.md).  
  

