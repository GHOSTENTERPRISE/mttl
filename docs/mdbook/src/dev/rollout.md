## Steps to Transition

1. Create a [gitlab tag](https://docs.gitlab.com/ee/topics/git/tags.html) for the legacy MTTL - done.
1. Work with CCV to see what/how we need to do to safely transition without causing outages from them. I reached out and they are creating a ticket to identify what will cause issues. Because they are using Mongo for everything, they should just need to change the location of the before script on their end.
    - We could just make another script in the expected location that calls the before_script in the new location?
1. VTR Script needs to be updated to use the correct symlink too https://gitlab.com/90cos/cyt/training-systems/vtr-system/-/issues/60
1. Announce to all stakeholders that the MTTL will be down for 1 day (I don't believe that the MTTL will be down for a full day, but it would probably be wise to over-estimate in case of unforeseen issues)
    - Perhaps on that day Dr. Davis and I could have approval rights on the MTTL?
1. Copy the files over
1. Copy any CI Vars over
1. Ensure that the MTTL review app deploys to aws k8s without further changes (it should)
1. Merge to master
1. Use the below javascript code to redirect any links to the MTTL that specify a work role