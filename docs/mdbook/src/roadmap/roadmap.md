## Work Role Roadmaps

The work role roadmap pages provide learners step-by-step guidance on topics to study and actions necessary to achieve mastery in the knowledge, skills, abilities and tasks required to effectively preform the duties and responsibilities of a crew member with that work role. The steps outlined in the work role roadmap should also help learners prepare for any relevant exams required in order to earn the desired work role.

### The Roadmap is Interactive!

- Hovering over a roadmap step shows more information about that step.
- Most steps are clickable! Clicking on a step will provide you with relevant resources in a new tab.
- Selecting "Return to Overview" will take you back to the [work role overview page](./overview.md)

### Data Definitions

The data for the work role overview nodes are provided in the [work-role json files](../files/work-roles.md). Data is provided as an array of steps **that are arranged in the order in which they appear**, with element 0 appearing first on the roadmap 1 appearing second, and so on. This data is compiled into the roadmap.json file using the roadmap script (./mttl/scripts/build/roadmap_data.py). To define a new step, you will use this data structure:

```json
"paths": [
    {
        "name": "Commercial Path",
        "path": [
            {
                "name": "Request CSPO Training",
                "description": "Request Certified Scrum Product Owner and Certification",
                "link": "https://jira.90cos.cdl.af.mil/servicedesk/customer/portal/11"
            },
            {
                ...
            }
        ]
    },
    {
        "name": "Second Path",
        "path": [...]
    }
]
```

Using this data structure, there can be more than one path to earning a workrole, with each path offering potentially different (or overlapping) steps. Each path will appear as a tab in the workrole roadmaps screen.

**`Paths` data fields:**   
`name`: The name of the path. This name should be relatively short, as it will appear as the title of the tabs on the roadmap page.   
`path`: An array of path data that defines the steps that need to be taken should a learner decide to take that path.

**`Path` data fields:**   
`name`: The name of the step on the roadmap. This should also be relatively short, as it will appear on the roadmap box.   
`description`: A 1-3 sentence description of the step.   
`link`: The link that opens if a user clicks on that step. If there is no link for that step, defining the link as either `""` or `"#"` will prevent the step from having a link.
