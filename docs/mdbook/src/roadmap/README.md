## Work role Roadmap

The work role roadmap provides users with an overview of the progression of work roles within the organization, and allows members to track and plan their progression in their careers in two views:

1. The [overview page](./overview.md), which provides a top-level view of the progression **between work roles**.
2. The [work role roadmap](./roadmap.md) page, which provides an in-depth view of training required to obtain a work role.
