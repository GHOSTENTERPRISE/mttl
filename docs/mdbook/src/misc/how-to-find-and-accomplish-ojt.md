# How to find and accomplish (OJT) on the job training
1. **Reasoning behind having an on-the-job training (OJT) checklist.**
    * Provide a definitive list of items to achieve a work role.
    * In order to fully satisfy all the requirements for a work role/specialization, some training/evaluations must be done "on the job."
    * When a KSAT item is not covered in training and/or does not have an evaluation, the trainee must demonstrate that KSAT item in the performance of their job.
2. **Logic in creating an OJT checklist.**
    * An OJT checklist will be created for KSAT items that are:
        1.  not covered in training but have an evaluation,
        2.  not covered in evaluations but have training available, and
        3.  not covered in evaluations and do not have training available.
3. **The intended use for the OJT checklist is to be printed out and used on the job to document and work towards a certain work role.**
    * The supervisor/trainer will assign a trainee a certain area of training to learn in order to gain the required knowledge of the KSAT item.
    * The supervisor/trainer will assign a trainee a task to perform in order to demonstrate the KSAT item.
    * Once the trainee has demonstrated the capability described in the KSAT item, the trainer and trainee will sign-off to document the KSAT was achieved.
4. CYT is currently developing and revising OJT checklists.  More information will be provided through the [Training Road Map](https://https://90cos-mttl.90cos.com/roadmap/) feature of the MTTL website as it becomes available.  
